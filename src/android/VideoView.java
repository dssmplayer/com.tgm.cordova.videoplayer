/**
 * Copyright (C) 2016 Zidoo (www.zidoo.tv)
 * Created by : jiangbo@zidoo.tv
 */

package com.tgm.cordova.videoplayer;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class VideoView extends SurfaceView {
	private Uri							mUri						= null;
	private String						mUrl						= null;
	private SurfaceHolder				mSurfaceHolder				= null;
	public MediaPlayer					mMediaPlayer				= null;
	private OnPreparedListener			mOnPreparedListener			= null;
	private OnErrorListener				mOnErrorListener			= null;
	private OnInfoListener				mOnInfoListener				= null;
	private OnBufferingUpdateListener	mOnBufferingUpdateListener	= null;
	private OnCompletionListener		mOnCompletionListener			= null;
	private OnSeekCompleteListener		mOnSeekCompleteListener		= null;
	private Context						mContext					= null;
	private boolean						mIsRealtimeMedia			= false;
	private final String				LOG_TAG						= "VideoView";

	public VideoView(Context context) {
		super(context);
		initVideoView(context);
	}

	public VideoView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		initVideoView(context);
	}

	public VideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initVideoView(context);
	}

	private void initVideoView(Context ctx) {
		mContext = ctx;
		getHolder().addCallback(mSHCallback);
		requestFocus();
		if (ctx instanceof Activity)
			((Activity) ctx).setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	public void setVideoUrl(String url) {
		try {
			mIsRealtimeMedia = url.startsWith("rtsp:") || url.startsWith("rtmp:") || url.endsWith(".m3u8") || url.endsWith(".mpd");
			mUri = Uri.parse(url);
			mUrl = url;
			openVideo();
		} catch (Exception e) {
		}
	}

	public synchronized void stopPlayback() {
		Log.d(LOG_TAG, "stop Playback");
		if (mMediaPlayer != null) {
			if(mMediaPlayer.isPlaying()){
				mMediaPlayer.stop();
			}
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	public synchronized void seekTo(int msec) throws Exception{
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			//Log.d(LOG_TAG, "seekTo ...: " + msec);
			mMediaPlayer.pause();
			mMediaPlayer.seekTo(msec);
		}else{
			throw new Exception("seekTo in illegal state.");
		}
	}

	public void start() {
		if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
			Log.d(LOG_TAG, "start Playback");
			mMediaPlayer.start();
		}
	}

	public void restart() {
		if (mMediaPlayer != null) {
			mMediaPlayer.seekTo(0);
			mMediaPlayer.start();
		}
	}

	public void pause() {
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mMediaPlayer.pause();
		}
	}

	private void changePlayer(){
		try {
			Method useRTMediaPlayer=MediaPlayer.class.getDeclaredMethod("useRTMediaPlayer", Integer.TYPE);
			useRTMediaPlayer.setAccessible(true);
			useRTMediaPlayer.invoke(mMediaPlayer, 2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		if (am.isSpeakerphoneOn()) {
			am.setSpeakerphoneOn(false);
		}
		am.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		am.setStreamVolume(AudioManager.STREAM_VOICE_CALL,0, AudioManager.FLAG_PLAY_SOUND); 
		//mMediaPlayer.setAudioStreamType(streamtype);
		*/
	}

	private void openVideo() {
		if (mUri == null || mSurfaceHolder == null)
			return;
		// Intent i = new Intent("com.android.music.musicservicecommand");
		// i.putExtra("command", "pause");
		// mContext.sendBroadcast(i);
		try {
			mMediaPlayer = new MediaPlayer();

			if(!mIsRealtimeMedia){
				changePlayer();
			}

			mMediaPlayer.setOnPreparedListener(mPreparedListener);
			mMediaPlayer.setOnErrorListener(mErrorListener);
			mMediaPlayer.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
			mMediaPlayer.setOnInfoListener(mOnInfoListener);
			if(mIsRealtimeMedia){
				mMediaPlayer.setDataSource(mUrl);
			}else{
				mMediaPlayer.setDataSource(mContext, mUri);
			}
			mMediaPlayer.setDisplay(mSurfaceHolder);
			mMediaPlayer.setOnCompletionListener(mCompletionListener);
			mMediaPlayer.setOnSeekCompleteListener(mSeekCompleteListener);
			mMediaPlayer.setOnVideoSizeChangedListener(onVideoSizeChangedListener);
			mMediaPlayer.setScreenOnWhilePlaying(true);
			mMediaPlayer.prepareAsync();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	OnPreparedListener	mPreparedListener	= new OnPreparedListener() {
		public void onPrepared(MediaPlayer mp) {
			start();
		}
	};

	OnErrorListener mErrorListener = new OnErrorListener() {
		public boolean onError(MediaPlayer mp, int what, int extra) {
			stopPlayback();
			Log.e(LOG_TAG, "onError(" + what + ", " + extra + ")");
			return true;
		}
	};

	OnCompletionListener mCompletionListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			mMediaPlayer.pause();
			Log.d(LOG_TAG, "onCompletion");
		}
	};

	OnSeekCompleteListener mSeekCompleteListener = new OnSeekCompleteListener() {
		public void onSeekComplete(MediaPlayer mp) {
			mMediaPlayer.start();
			//Log.d(LOG_TAG, "onSeekComplete");
		}
	};

	public void setOnSeekCompleteListener(OnSeekCompleteListener l) {
		mOnSeekCompleteListener = l;
	}

	public void setOnCompletionListener(OnCompletionListener l) {
		mOnCompletionListener = l;
	}

	public void setOnPreparedListener(OnPreparedListener l) {
		mOnPreparedListener = l;
	}

	public void setOnErrorListener(OnErrorListener l) {
		mOnErrorListener = l;
	}

	public void setOnBufferingUpdateListener(OnBufferingUpdateListener l) {
		mOnBufferingUpdateListener = l;
	}

	public void setOnInfoListener(OnInfoListener l) {
		mOnInfoListener = l;
	}

	SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback() {
		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
			// holder.setFixedSize(w,
			// h);
		}

		public void surfaceCreated(SurfaceHolder holder) {
			mSurfaceHolder = holder;
			if (mMediaPlayer != null) {
				mMediaPlayer.setDisplay(mSurfaceHolder);
			} else {
				if (mUri != null) {
					openVideo();
				}
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			mSurfaceHolder = null;
		}
	};

	OnVideoSizeChangedListener onVideoSizeChangedListener = new OnVideoSizeChangedListener() {
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
		}
	};
}
