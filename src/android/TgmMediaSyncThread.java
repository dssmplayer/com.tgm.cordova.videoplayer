/**
 * @author      : crow (crow1013_at_gmail.com)
 * @file        : TgmMediaSyncThread
 * @created     : Wednesday Feb 22, 2023 15:46:54 KST
 */

package com.tgm.cordova.videoplayer;

import java.io.IOException;
import java.nio.ByteBuffer;

import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Surface;
import android.widget.VideoView;

import android.media.MediaSync;
import android.media.PlaybackParams;

import android.media.AudioFormat;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.AudioTrack;

import org.apache.cordova.CallbackContext;
import static android.media.MediaCodecInfo.CodecProfileLevel.*;

public class TgmMediaSyncThread extends Thread {
	private static final String TAG = "TgmMediaSyncThread";

	private Surface mSurface;
	private String mMediaPath;
	public TgmVideoSyncThread mVideoSyncThread = null;
	private TgmAudioSyncThread mAudioSyncThread = null;;
    private StartRenderListener mStartRenderListener = null;

	public interface StartRenderListener {
		public void onStartRender();
	}

	private boolean started = false;

	public boolean isStarted() {
		return started;
	}

	public TgmMediaSyncThread(Surface surface, String videoPath, StartRenderListener listener) {
		mSurface = surface;
		mMediaPath = videoPath;
		mStartRenderListener = listener;
	}

    @Override
	public void run() {
		started = false;
		boolean exit = false;
		super.run();
		Log.d(TAG, "TgmMediaSyncThread run");

		mVideoSyncThread = new TgmVideoSyncThread(mSurface, mMediaPath, mStartRenderListener);
		mVideoSyncThread.start();

		mAudioSyncThread = new TgmAudioSyncThread(mMediaPath);
		mAudioSyncThread.start();

		while(true && !exit) {
			if(mVideoSyncThread.started || mVideoSyncThread.failToStart){
				break;
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				Log.d(TAG, "TgmMediaSyncThread interrupted, in waiting mVideoSyncThread started");
				exit = true;
				break;
			}
		}

		while(true && !exit) {
			if(mAudioSyncThread.started || mAudioSyncThread.failToStart){
				break;
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				Log.d(TAG, "TgmMediaSyncThread interrupted, in waiting mAudioSyncThread started");
				exit = true;
				break;
			}
		}

		started = true;

		while(true && !exit){
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				//e.printStackTrace();
				Log.d(TAG, "TgmMediaSyncThread interrupted");
				break;
			}
		}
		mVideoSyncThread.stopThread();
		mAudioSyncThread.stopThread();

		try {
			mVideoSyncThread.join();
			mAudioSyncThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		mVideoSyncThread = null;
		mAudioSyncThread = null;
	}

	public void seekTo(int msec) {
		Log.d(TAG, "seekTo " + msec + " : " + mMediaPath);
		if (mVideoSyncThread != null) {
			mVideoSyncThread.seekTo(msec);
		}
		if(mAudioSyncThread != null){
			mAudioSyncThread.seekTo(msec);
		}
	}

	public void setStartMS(long startMS){
		Log.d(TAG, "setStartMS " + startMS + " : " + mMediaPath);
		if (mVideoSyncThread != null) {
			mVideoSyncThread.setStartMS(startMS);
		}
		if(mAudioSyncThread != null){
			mAudioSyncThread.setStartMS(startMS);
		}
	}

	public void hide(){
		if (mVideoSyncThread != null) {
			mVideoSyncThread.hide();
		}
		if(mAudioSyncThread != null){
			mAudioSyncThread.hide();
		}
	}

	public void show(){
		if (mVideoSyncThread != null) {
			mVideoSyncThread.show();
		}
		if(mAudioSyncThread != null){
			mAudioSyncThread.show();
		}
	}
}

class TgmVideoSyncThread extends Thread {
    public static final String TAG = "TgmMediaSyncThread::Video";
    protected MediaExtractor mExtractor;
    protected MediaCodec mDecoder;
    private Surface mSurface;
    protected String mMediaPath;
    protected boolean eosThread = false;
    //private int outOfTimeThreshold = 30;//ms
    private int outOfTimeThreshold = 0;//ms
    public int flag = 0;
	protected long startMS = 0;//ms
    public boolean started = false;
	public boolean failToStart = false;
    //public long curTimeStamp = 0;
    private int terminateTimeout = 3;
    private boolean isEOS = false;
    protected BufferInfo mDecoderInfo;
    private boolean isCodecErr = false;
    private boolean mStartRelease = false;
	private final int BUFFER_TIMEOUT = 0;
	private long mCountInQueue = 0;
	private long mCountOutQueue = 0;
	private TgmMediaSyncThread.StartRenderListener mStartRenderListener = null;
	private int mSeek = 0;
	private boolean mTriggerSeekTo = true;

	private final Object lock1 = new Object();
   
    public TgmVideoSyncThread(Surface surface, String path, TgmMediaSyncThread.StartRenderListener listener) {
        this.mSurface = surface;
        this.mMediaPath = path;
		this.mStartRenderListener = listener;
    }

	protected synchronized boolean setUp(){
        Log.d(TAG, "setUp() start for: " + mMediaPath);
        mExtractor = new MediaExtractor();
        try {
            mExtractor.setDataSource(mMediaPath);
        } catch (IOException e1) {
            e1.printStackTrace();
            Log.v(TAG, "mExtractor.setDataSource != error");
            return false;
        }
        Log.d(TAG, "End setDataSource ... " + mMediaPath);

		MediaFormat format = null;

        for (int i = 0; i < mExtractor.getTrackCount(); i++) {
            format = mExtractor.getTrackFormat(i);
			String mime = format.getString(MediaFormat.KEY_MIME);
			Log.d(TAG, "extracted MediaTrack : " + i + ":" + mime + ":" + format);
			if (mime.startsWith("video/")) {
				mExtractor.selectTrack(i);
				try{
					mDecoder = MediaCodec.createDecoderByType(mime);
				} catch (Exception e) {
					Log.e(TAG, "!!!Create mDecoder video codec from type ERROR!!! : " + mime);
					break;
				}
				mDecoder.configure(format, mSurface, null, 0);
				//mDecoder.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
				break;
			}
		}
		if(mDecoder == null){
            Log.v(TAG, "cannot create video decoder");
			return false;
		}

		mDecoderInfo = new BufferInfo();

		/*
		//////////////////////////////////////////
		mDecoder.setCallback(new MediaCodec.Callback(){
			@Override
			public void onInputBufferAvailable(MediaCodec codec, int index){
				ByteBuffer inputBuffer = mDecoder.getInputBuffer(index);
				inputBuffer.clear();
				int sampleSize = mExtractor.readSampleData(inputBuffer, 0);

				if(sampleSize > 0){
					boolean isOver = !mExtractor.advance();
					mDecoder.queueInputBuffer(index, 0, sampleSize, mExtractor.getSampleTime(), 0);
				}
			}

			@Override
			public void onOutputBufferAvailable(MediaCodec codec, int index, BufferInfo info){
			}

			@Override
			public void onOutputFormatChanged(MediaCodec codec, MediaFormat format){
			}

			@Override
			public void onError(MediaCodec codec, MediaCodec.CodecException e){
				Log.e(TAG, "onError : " + e);
				//isCodecErr = true;
			}
		});
		//////////////////////////////////////////////
		*/

        mDecoder.start();
		started = true;
        Log.d(TAG, "setUp() complete for: " + mMediaPath + " : " + format);
		return true;
	}

	private synchronized void tearDown(){
		Log.d(TAG, "tearDown() start for: " + mMediaPath);
		releaseResource();
		Log.d(TAG, "tearDown() complete for: " + mMediaPath);
	}

	protected void releaseBufferToDecoder(int index){
		mDecoder.releaseOutputBuffer(index, true);
	}

   	protected void skipBufferToDecoder(int index){
		mDecoder.releaseOutputBuffer(index, false);
	}

	@Override
	public void run() {
		super.run();
		final long kTimeOutUs = 1000000;  // 1 second
		if(!setUp()){
			tearDown();
			failToStart = true;
			return;
		}
		boolean exit = false;
		boolean startRender = false;

		//while(!isInterrupted() && !exit && !eosThread){
		while(!eosThread && !exit){
			if(startMS <= 0){
				try {
					sleep(10);
				}catch(Exception  ex) {
				}
				continue;
			}

			if(mTriggerSeekTo){
				Log.d(TAG, "start to seekTo " + mSeek + " : " + mMediaPath);
				mExtractor.seekTo(mSeek * 1000, MediaExtractor.SEEK_TO_CLOSEST_SYNC);

				// drain all the output codec buffer
				mDecoder.flush();

				/*
				while(true){
					int index = mDecoder.dequeueOutputBuffer(mDecoderInfo, 0);
					if(index >= 0){
						if((mDecoderInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0){
							Log.d(TAG, "drain all the codec buffer, EOS");
							break;
						}
						if(mDecoderInfo.presentationTimeUs / 1000 >= mSeek){
							Log.d(TAG, "drain all the codec buffer, seek to " + mSeek + " ms");
							break;
						}
						mDecoder.releaseOutputBuffer(index, false);
					}else{
						break;
					}
				}
				*/

				mTriggerSeekTo = false;
				Log.d(TAG, "complete to seekTo " + mSeek + " : " + mMediaPath);
			}

			int inIndex = mDecoder.dequeueInputBuffer(BUFFER_TIMEOUT);
			//int inIndex = mDecoder.dequeueInputBuffer(kTimeOutUs);

			//Log.d(TAG, "inIndex = " + inIndex);
			if(inIndex >= 0){
				ByteBuffer inputBuffer = mDecoder.getInputBuffer(inIndex);
				int sampleSize = mExtractor.readSampleData(inputBuffer, 0);
				if(sampleSize < 0){
					mDecoder.queueInputBuffer(inIndex, 0, 0, 0, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
					//exit = true;
				}else{
					mDecoder.queueInputBuffer(inIndex, 0, sampleSize, mExtractor.getSampleTime(), 0);
					//mDecoder.queueInputBuffer(inIndex, 0, sampleSize, 0, 0);
					mExtractor.advance();
				}
			}

			int outIndex = mDecoder.dequeueOutputBuffer(mDecoderInfo, BUFFER_TIMEOUT);
			//Log.d(TAG, "outIndex = " + outIndex);

			//int outIndex = mDecoder.dequeueOutputBuffer(mDecoderInfo, kTimeOutUs);
			if(outIndex >= 0){
				if((mDecoderInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0){
					Log.d(TAG, "meet BUFFER_FLAG_END_OF_STREAM");
					exit = true;
				}else{
					long pts = mDecoderInfo.presentationTimeUs/1000;
					final long delay = pts - (System.currentTimeMillis() - startMS);

					if(delay <= 0){
						//skipBufferToDecoder(outIndex);	// blink video at heavy video stream
						releaseBufferToDecoder(outIndex);	// slowmotion at heavy video stream
					}else{
						/*
						new Handler().postDelayed(new Runnable() {
							@Override
							public void run() {
								Log.d("", "$");
								releaseBufferToDecoder(outIndex);
							}
						}, delay);
						*/
						/*
						while((pts > (System.currentTimeMillis() - startMS)) && !eosThread) {
							try {
								sleep(1);
							} catch (InterruptedException e) {
								//e.printStackTrace();
								break;
							}
						}
						*/
						try {
							sleep(delay);
						} catch (InterruptedException e) {
							//e.printStackTrace();
						}
						releaseBufferToDecoder(outIndex);
					}

					if(!startRender){
						startRender = true;
						if(mStartRenderListener != null){
							mStartRenderListener.onStartRender();
						}
					}
				}
			}else if(outIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED){
				Log.d(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
				mDecoder.getOutputBuffers();
				//mDecoder.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
			}else if(outIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED){
				Log.d(TAG, "New format " + mDecoder.getOutputFormat());
				MediaFormat format = mDecoder.getOutputFormat();
				//mDecoder.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
			}
		}

		tearDown();
    }

    public void releaseResource()
    {
        if(eosThread == false)
            eosThread = true;
        if(mStartRelease == false) {
            mStartRelease = true;
            Log.d(TAG, "Video Thread start releasing !! " + mMediaPath);

            if(mDecoder != null){
                if(isCodecErr == false) {
                    mDecoder.flush();
                    mDecoder.stop();
                }
                mDecoder.release();
				mDecoder = null;
            }

            if(mExtractor != null){
                mExtractor.release();
				mExtractor = null;
			}
			flag = -1;
        }
        Log.d(TAG, "Thread normally Stop !! " + mMediaPath);
		started = false;
    }

	public synchronized void stopThread() {
		eosThread = true;
	}

	public void seekTo(int msec) {
		mSeek = msec;
		mTriggerSeekTo = true;
			/*
		synchronized(lock1){
			while(true){
				int index = mDecoder.dequeueInputBuffer(BUFFER_TIMEOUT);
				if(index >= 0){
					mDecoder.releaseOutputBuffer(index, false);
					if((mDecoderInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0){
						break;
					}
				}else{
					break;
				}
			}
			if(mExtractor != null){
				mExtractor.seekTo(msec * 1000, MediaExtractor.SEEK_TO_CLOSEST_SYNC);
			}
		}
			*/
	}

	public void setStartMS(long startMS){
		Log.d(TAG, "setStartMS " + startMS + " : " + mMediaPath);
		synchronized(lock1){
			this.startMS = startMS;
		}
	}

	public synchronized void hide(){
	}

	public synchronized void show(){
	}
}

class TgmAudioSyncThread extends TgmVideoSyncThread{
    public static final String TAG = "TgmMediaSyncThread::Audio";
    private int bufferSize;
    private AudioTrack mAudioTrack = null;
	private boolean muteFlag = false;

    public TgmAudioSyncThread(String path) {
		super(null, path, null);
        this.mMediaPath = path;
    }

	public void hide(){
		mute();
	}

	public void show(){
		unmute();
	}

	protected boolean setUp(){
        Log.d(TAG, "setUp() start for: " + mMediaPath);
        mExtractor = new MediaExtractor();
        try {
            mExtractor.setDataSource(mMediaPath);
        } catch (IOException e1) {
            e1.printStackTrace();
            Log.v(TAG, "mExtractor.setDataSource != error");
            return false;
        }
        Log.d(TAG, "End setDataSource ... " + mMediaPath);

		MediaFormat format = null;

        for (int i = 0; i < mExtractor.getTrackCount(); i++) {
            format = mExtractor.getTrackFormat(i);
			String mime = format.getString(MediaFormat.KEY_MIME);
			Log.d(TAG, "extracted MediaTrack : " + i + ":" + mime);

			if (mime.startsWith("audio/")) {
				mExtractor.selectTrack(i);
				try{
					mDecoder = MediaCodec.createDecoderByType(mime);
				} catch (Exception e) {
					Log.e(TAG, "!!!Create mDecoder audio codec from type ERROR!!! : " + mime);
					break;
				}
				mDecoder.configure(format, null, null, 0);
				break;
			}
		}
		if(mDecoder == null){
            Log.v(TAG, "cannot create video decoder");
			return false;
		}

        int channelCount = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
        int channelMaskConfig = 0;

        switch(channelCount)
        {
            case 1:
                channelMaskConfig = AudioFormat.CHANNEL_OUT_MONO;
                break;
            case 2:
                channelMaskConfig = AudioFormat.CHANNEL_OUT_STEREO;
                break;
            case 3:
                channelMaskConfig = (AudioFormat.CHANNEL_OUT_STEREO | AudioFormat.CHANNEL_OUT_FRONT_CENTER);
                break;
            case 4:
                channelMaskConfig = AudioFormat.CHANNEL_OUT_QUAD;
                break;
            case 5:
                channelMaskConfig = (AudioFormat.CHANNEL_OUT_QUAD | AudioFormat.CHANNEL_OUT_FRONT_CENTER);
                break;
            case 6:
                channelMaskConfig = AudioFormat.CHANNEL_OUT_5POINT1;
                break;
            case 7:
                channelMaskConfig = (AudioFormat.CHANNEL_OUT_5POINT1 | AudioFormat.CHANNEL_OUT_BACK_CENTER);
                break;
            case 8:
            	//AudioFormat.CHANNEL_OUT_7POINT1_SURROUND
                channelMaskConfig = AudioFormat.CHANNEL_OUT_7POINT1_SURROUND;
                break;
            default:
                channelMaskConfig = AudioFormat.CHANNEL_OUT_STEREO;
                break;
        }
        int sampleRate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE);
        int minBufferSize = AudioTrack.getMinBufferSize(sampleRate,
                channelMaskConfig,
                AudioFormat.ENCODING_PCM_16BIT);

        bufferSize = 4 * minBufferSize;

		mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
				sampleRate,
				channelMaskConfig,
				AudioFormat.ENCODING_PCM_16BIT,
				bufferSize, AudioTrack.MODE_STREAM);

		try{
			mAudioTrack.play();
		}catch(Exception e){
            Log.e(TAG, "AudioTrack play ERROR !!! " + e);
			e.printStackTrace();
			return false;
		}

        mDecoder.start();
        mDecoderInfo = new BufferInfo();
		started = true;
        Log.d(TAG, "setUp() complete for: " + mMediaPath + " : " + format);
		return true;
	}

	protected void releaseBufferToDecoder(int index){
		ByteBuffer outputByteBuffer = mDecoder.getOutputBuffer(index);

		if(mDecoderInfo.size > 0 && !muteFlag){
			byte[] chunk = new byte[mDecoderInfo.size];
			outputByteBuffer.get(chunk);
			outputByteBuffer.clear();
			mAudioTrack.write(chunk, 0, chunk.length);
		}

		mDecoder.releaseOutputBuffer(index, false);
	}

	private void mute(){
		muteFlag = true;
	}

	private void unmute(){
		muteFlag = false;
	}

}
