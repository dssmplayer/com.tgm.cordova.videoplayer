package com.tgm.cordova.videoplayer;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
//import android.widget.VideoView;
//import com.tgm.cordova.videoplayer.VideoView;
import android.util.DisplayMetrics;
import android.graphics.PixelFormat;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaResourceApi;
import org.apache.cordova.PluginResult;
import org.json.JSONException;
import org.json.JSONObject;

public class VideoPlayerRealtek extends CordovaPlugin{

	protected static final String LOG_TAG = "VideoPlayerVideoView";
	protected static final String ASSETS = "/android_asset/";
	private CallbackContext callbackContext = null;
	private Dialog dialog;
	private VideoView videoView = null;
	//private MediaPlayer player;

	/**
	 * Executes the request and returns PluginResult.
	 *
	 * @param action        The action to execute.
	 * @param args          JSONArray of arguments for the plugin.
	 * @param callbackId    The callback id used when calling back into JavaScript.
	 * @return              A PluginResult object with a status and message.
	 */
	public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
		if (action.equals("play")) {
			this.callbackContext = callbackContext;

			CordovaResourceApi resourceApi = webView.getResourceApi();
			String target = args.getString(0);
			final JSONObject options = args.getJSONObject(1);

			String fileUriStr;
			try {
				Uri targetUri = resourceApi.remapUri(Uri.parse(target));
				fileUriStr = targetUri.toString();
			} catch (IllegalArgumentException e) {
				fileUriStr = target;
			}

			Log.v(LOG_TAG, fileUriStr);

			final String path = stripFileProtocol(fileUriStr);

			// Create dialog in new thread
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					//openVideoDialog(path, options);
					openVideoDialogCrow(path, options);
				}
			});

			// Don't return any result now
			PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
			pluginResult.setKeepCallback(true);
			callbackContext.sendPluginResult(pluginResult);
			callbackContext = null;

			return true;
		}
		else if (action.equals("close")) {
			Log.d(LOG_TAG, "close");
			if(videoView != null){
				videoView.stopPlayback();
			}
			if (dialog != null) {
				dialog.dismiss();
			}

			if (callbackContext != null) {
				PluginResult result = new PluginResult(PluginResult.Status.OK);
				result.setKeepCallback(false); // release status callback in JS side
				callbackContext.sendPluginResult(result);
				callbackContext = null;
			}

			return true;
		}
		return false;
	}

	/**
	 * Removes the "file://" prefix from the given URI string, if applicable.
	 * If the given URI string doesn't have a "file://" prefix, it is returned unchanged.
	 *
	 * @param uriString the URI string to operate on
	 * @return a path without the "file://" prefix
	 */
	public static String stripFileProtocol(String uriString) {
		if (uriString.startsWith("file://")) {
			return Uri.parse(uriString).getPath();
		}
		return uriString;
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	protected void openVideoDialogCrow(String path, JSONObject options) {
		// Let's create the main dialog
		dialog = new Dialog(cordova.getActivity(), android.R.style.Theme_NoTitleBar);
		dialog.getWindow().getAttributes().windowAnimations = android.R.style.Animation_Dialog;
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

		dialog.setCancelable(true);

		dialog.setOnDismissListener(new OnDismissListener(){
			@Override
			public void onDismiss(DialogInterface dialog) {
				Log.d(LOG_TAG, "Dialog dismissed");
				if (callbackContext != null) {
					PluginResult result = new PluginResult(PluginResult.Status.OK);
					result.setKeepCallback(false); // release status callback in JS side
					callbackContext.sendPluginResult(result);
					callbackContext = null;
				}
			}
		});
			
		dialog.getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN, LayoutParams.FLAG_FULLSCREEN);

		// Main container layout
		LinearLayout main = new LinearLayout(cordova.getActivity());
		main.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		main.setOrientation(LinearLayout.VERTICAL);
		main.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
		main.setVerticalGravity(Gravity.CENTER_VERTICAL);

		main.setClickable(false);
		main.setFocusable(false);

		videoView = new VideoView(cordova.getActivity());

		videoView.setClickable(false);
		videoView.setFocusable(false);

		videoView.setOnPreparedListener(new OnPreparedListener(){
			@Override
			public void onPrepared(MediaPlayer mp){
				Log.d(LOG_TAG, "onPrepared");
				mp.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
				videoView.start();
				//mp.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
			}
		});

		videoView.setOnErrorListener(new OnErrorListener(){
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				Log.e(LOG_TAG, "onError(" + what + ", " + extra + ")");
				//videoView.suspend();
				videoView.stopPlayback();
				dialog.dismiss();
				return false;
			}
		});

		videoView.setOnCompletionListener(new OnCompletionListener(){
			@Override
			public void onCompletion(MediaPlayer mp) {
				Log.d(LOG_TAG, "onCompletion");
				//videoView.suspend();
				videoView.stopPlayback();
				dialog.dismiss();
			}
		});

		videoView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		//videoView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		// videoView.setVideoURI(uri);
		
		Log.d(LOG_TAG, "set video uri: " + path);
		//videoView.setVideoURI(Uri.parse(path));
		videoView.setVideoUrl(path);
		//videoView.setVideoPath(Uri.parse(path));

		main.addView(videoView);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(dialog.getWindow().getAttributes());
		lp.flags = WindowManager.LayoutParams.FLAG_FULLSCREEN
			| WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
			| WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
			| WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
			| WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
			| WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN;

		try {
			//DisplayMetrics dm = new DisplayMetrics();
			//this.cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

			//float xRes = (float)dm.widthPixels;
			//float yRes = (float)dm.heightPixels;
			float xRes = Float.valueOf(options.getString("screenWidth"));
			float yRes = Float.valueOf(options.getString("screenHeight"));
			float width = Float.valueOf(options.getString("width"));
			float height = Float.valueOf(options.getString("height"));
			float top =  Float.valueOf(options.getString("top"));
			float left = Float.valueOf(options.getString("left"));

			lp.x = (int)(xRes * (left/100.0));
			lp.y = (int)(yRes * (top/100.0));
			lp.width = (int)(xRes * (width/100.0));
			lp.height = (int)(yRes * (height/100.0));
			lp.gravity = Gravity.LEFT | Gravity.TOP;

		} catch (Exception e) {
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		}

		Log.d(LOG_TAG, "left,top & width x height => " + lp.x + "," + lp.y + " & " +  lp.width + "x" + lp.height);

		dialog.setContentView(main);
		dialog.show();
		dialog.getWindow().setAttributes(lp);
	}
}



