package com.tgm.cordova.videoplayer;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface; 
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaActivity;

import android.util.Log;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.CopyOnWriteArrayList;

public class VideoPlayerMulti extends CordovaPlugin {
	protected String TAG = "TgmVideoPlayerMulti";
	//private List<VideoPlayer> videoPlayers = new ArrayList<VideoPlayer>();
	private CopyOnWriteArrayList<VideoPlayer> videoPlayers = new CopyOnWriteArrayList<VideoPlayer>();
	private int lastId = 1_000;	// CordovaWebView id starts from 100, then id from 1_000 may be safe
	private final int maxLastId = 65535;	// 2^16 - 1

	@Override
	public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
		final int targetId = args.getInt(0);
		Log.d(TAG, "action: " + action + ", id: " + targetId);
		JSONArray newArray = new JSONArray();

		if(action.equals("isOpened")){
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run(){
					final int checkedId = checkOpened(targetId);
					if(checkedId >= 0){
						callbackContext.success(checkedId);
					}else{
						callbackContext.error("VideoPlayer not found");
					}
				}
			});
			return true;
		}else if(action.equals("open")){
//			cordova.getThreadPool().execute(new Runnable() {
//			//cordova.getActivity().runOnUiThread(new Runnable() {
//				@Override
//				public void run(){
					final int fdId = VideoPlayerMulti.this.open(targetId);
					if(fdId >= 0){
						callbackContext.success(fdId);
					}else{
						callbackContext.error("VideoPlayer cannot be created");
					}
//				}
//			});
			return true;
		}else if(action.equals("close")){
			cordova.getThreadPool().execute(new Runnable() {
				@Override
				public void run(){
					VideoPlayerMulti.this.close(targetId);
					callbackContext.success(targetId);
				}
			});
			return true;
		}else if(action.equals("isOK")){
		}else if(action.equals("isRtspDisconnected")){
		}else if(action.equals("hide")){
		}else if(action.equals("show")){
		}else if(action.equals("gotoStartingPoint")){
		}else if(action.equals("seekTo")){
			newArray.put(args.get(1));
			newArray.put(args.get(2));
		}else if(action.startsWith("play")) {
			newArray.put(args.get(1));
			newArray.put(args.get(2));
		}else if(action.equals("changeVideoSource")){
			newArray.put(args.get(1));
		}else{
			callbackContext.error("Invalid action");
			return false;
		}

		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run(){
				try {
					CordovaArgs newArgs = new CordovaArgs(newArray);
					VideoPlayer vp = getVideoPlayer(targetId);
					if(vp == null){
						Log.d(TAG, "VideoPlayer not found, action: " + action + ", targetId: " + targetId);
						callbackContext.error("VideoPlayer not found");
						return;
					}
					vp.execute(action, newArgs, callbackContext);
					return;
				} catch (JSONException e) {
					Log.d(TAG, "execute error: " + e.getMessage());
					callbackContext.error("execute error: " + e.getMessage());
					return;
				}
			}
		});

		return true;
	}

	private int checkOpened(final int id) {
		Log.d(TAG, "checkOpened");
		for (VideoPlayer vp : videoPlayers) {
			Log.d(TAG, "checkOpened getId(): " + vp.getId());
			if (vp.getId() == id) {
				return id;
			}
		}
		return -1;
	}

	private synchronized int open(final int id) {
		Log.d(TAG, "open, id: " + id);
		for (VideoPlayer vp : videoPlayers) {
			if (vp.getId() == id) {
				Log.d(TAG, "VideoPlayer already exists");
				return id;
			}
		}
		lastId = ++lastId % maxLastId;
		VideoPlayer vp = new VideoPlayer(lastId, cordova.getActivity(), webView);
		if(vp == null){
			Log.d(TAG, "cannot create VideoPlayer");
			return -1;
		}
		videoPlayers.add(vp);
		Log.d(TAG, "open, VideoPlayer created, id: " + lastId + ", count: " + videoPlayers.size() + ", ids: " + Arrays.toString(videoPlayers.stream().map(v -> v.getId()).toArray()));
		//Log.d(TAG, "open, VideoPlayer created, id: " + lastId + ", count: " + videoPlayers.size());
		return lastId;
	}

	private void close(final int id) {
		Log.d(TAG, "close, id: " + id);
		for (VideoPlayer vp : videoPlayers) {
			if (vp.getId() == id) {
				vp.close();
				videoPlayers.remove(vp);
				vp = null;
				Log.d(TAG, "close, VideoPlayer closed, id: " + id + ", count: " + videoPlayers.size() + ", ids: " + Arrays.toString(videoPlayers.stream().map(v -> v.getId()).toArray()));
				return;
			}
		}
		// display ids of videoPlayers
		Log.d(TAG, "close, cannot find id: " + id + ", count: " + videoPlayers.size() + ", ids: " + Arrays.toString(videoPlayers.stream().map(v -> v.getId()).toArray()));
		//Log.d(TAG, "close, VideoPlayer closed, id: " + id + ", count: " + videoPlayers.size());
		return;
	}

	private VideoPlayer getVideoPlayer(int id) {
		for (VideoPlayer vp : videoPlayers) {
			if (vp.getId() == id) {
				return vp;
			}
		}
		return null;
	}
}


