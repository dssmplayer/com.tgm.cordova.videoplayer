package com.tgm.cordova.videoplayer;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.os.SystemClock;

import android.view.Surface;
import android.view.TextureView;
import android.graphics.SurfaceTexture;

import android.support.annotation.NonNull;
import com.yqritc.scalablevideoview.ScalableVideoView;
import com.yqritc.scalablevideoview.ScalableType;
import java.util.Date;
import org.apache.cordova.CallbackContext;
import android.os.Handler;
import android.os.Message;
import android.view.View;


public class TgmVideoView extends ScalableVideoView {
	//long mTriggerTimeMS = 0;
	long mStartTimeMS = 0;
	Handler mHandler = new Handler();
	ScalableType mScalableType = ScalableType.FIT_XY;
	boolean mPrepared = false;
	//String mUrl = null;
	Surface mSurface = null;
	CallbackContext mStartCallbackContext = null;

	private static final String TAG = "TgmVideoView";

	public TgmVideoView(Context context) {
        super(context, null);
    }

	@Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        mSurface = new Surface(surfaceTexture);
		Log.d(TAG, "onSurfaceTextureAvailable, size: " + width + "x" + height);
        if (mMediaPlayer != null) {
			Log.d(TAG, "SurfaceTexture attached to MediaPlayer");
            mMediaPlayer.setSurface(mSurface);
        }
    }

	@Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		Log.d(TAG, "onSurfaceTextureDestroyed");
		mSurface = null;
        return false;
    }

	public void initializeMediaPlayer(String url, ScalableType st, CallbackContext callbackContext) throws Exception {
		mScalableType = st;
		//mUrl = url;
		mStartCallbackContext = callbackContext;
		setVideoUrl(url);
		setLooping(false);
		setVolume(1, 1);
		setScalableType(mScalableType);

        if (mSurface != null) {
			Log.d(TAG, "SurfaceTexture attached to MediaPlayer");
            mMediaPlayer.setSurface(mSurface);
        }

		prepareAsync(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				Log.d(TAG, "onPrepared");
				mMediaPlayer.setPlaybackParams(mMediaPlayer.getPlaybackParams().setSpeed(1.0f));
				start();
				class SetPrepared implements Runnable {
					@Override
					public void run() {
						if(TgmVideoView.this.mMediaPlayer == null) return;
						if(TgmVideoView.this.mMediaPlayer.isPlaying()) {
							hide();
							stopTracking();

							mMediaPlayer.pause();
							mMediaPlayer.setOnSeekCompleteListener(null);
							mMediaPlayer.seekTo(0);

							TgmVideoView.this.mPrepared = true;
							callbackContext.success("onPrepare: OK");
						} else {
							TgmVideoView.this.mHandler.postDelayed(this, 30);
						}
					}
				}
				mHandler.postDelayed(new SetPrepared(), 30);
			}
		});
	}

	public void setVideoUrl(String url) {
		try {
			setDataSource(url);
			//mMediaPlayer.setOnSeekCompleteListener(mSeekCompleteListener);
			mMediaPlayer.setOnBufferingUpdateListener(mBufferingUpdateListener);
			mMediaPlayer.setOnCompletionListener(mCompletionListener);
			mMediaPlayer.setOnErrorListener(mErrorListener);
			mMediaPlayer.setOnInfoListener(mInfoListener);
			//mHandler = new Handler();
		} catch (Exception e) {
		}
	}

	public synchronized void stopPlayback() {
		Log.d(TAG, "stop Playback");
		mPrepared = false;
		if (mMediaPlayer != null) {
			if(mMediaPlayer.isPlaying()){
				mMediaPlayer.stop();
			}
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
	}

	@Override
	public void seekTo(int msec){
		mIsTracking = false;
		if(mMediaPlayer.isPlaying()){
			mMediaPlayer.pause();
		}
		mTriggerTimeSeek = System.currentTimeMillis();
		mMediaPlayer.seekTo(msec + 0, MediaPlayer.SEEK_CLOSEST);

		//mMediaPlayer.seekTo(msec + mLatencySeekTo, MediaPlayer.SEEK_CLOSEST);
		//mMediaPlayer.seekTo(msec + 0, MediaPlayer.SEEK_PREVIOUS_SYNC);
		//mMediaPlayer.seekTo(msec + 0, MediaPlayer.SEEK_CLOSEST_SYNC);
	}

	public void seekToAndPause(int msec) throws Exception{
		stopTracking();
		mMediaPlayer.start();
		mMediaPlayer.setOnSeekCompleteListener(null);

		class CheckStarted implements Runnable {
			@Override
			public void run() {
				if(TgmVideoView.this.mMediaPlayer == null) return;
				if(TgmVideoView.this.mMediaPlayer.isPlaying()) {
					mMediaPlayer.pause();
					mMediaPlayer.seekTo(msec + 0, MediaPlayer.SEEK_CLOSEST);
					Log.d(TAG, "seekToAndPause: OK, " + msec);
					return;
				} else {
					TgmVideoView.this.mHandler.postDelayed(this, 30);
				}
			}
		}
		mHandler.postDelayed(new CheckStarted(), 30);

	}

	// video timesync functions
	
	private boolean mIsTracking = false;
	private int mTrackerID = 0;
	private int mLatencySeekTo = 150;

	private int mLatencySpeedChange = 50;
	private int mTriggerTimeChangeSpeed = 0;
	private long mTriggerTimeSeek = 0;
	private long mStartLatency = 40;

	private final int dT = 50;
	private final int mInterval = 1000;
	private final float mMinSpeed = 0.1f;
	private final float mMaxSpeed = 10.0f;

	private final int mTooBigInterval = 30_000;
	private int mMinusWantedPosCounter = 0;
	private int minimumGap = 2; // msec

	private synchronized int tuneVideoSpeed(int trkId){
		//if(mMediaPlayer == null || !mMediaPlayer.isPlaying() || mStartTimeMS == 0){
		if(mMediaPlayer == null || !mPrepared || mStartTimeMS == 0){
			return 100;
		}

		int wantedPos = Long.valueOf(System.currentTimeMillis() - mStartTimeMS).intValue();
		if(wantedPos < 0){
			Log.d(TAG, "wantedPos: " + wantedPos + " < 0, wait for next round.");
			mMinusWantedPosCounter++;
			if(mMinusWantedPosCounter >= 10){
				Log.d(TAG, "wantedPos: " + wantedPos + " < 0, too many times, exit & restart app.");
				System.exit(0);
			}
			return Math.min(1000, -wantedPos);
		}
		mMinusWantedPosCounter = 0;

		int curPos = mMediaPlayer.getCurrentPosition();
		int gap = wantedPos - curPos;

		Log.d(TAG, "curPos: " + curPos + " wantedPos: " + wantedPos + " gap: " + gap);

		if(gap >= mTooBigInterval){
			// get duration of video
			int duration = mMediaPlayer.getDuration();
			if(wantedPos > duration){
				Log.d(TAG, "wantedPos: " + wantedPos + " > duration: " + duration + "!!, so exit & restart app.");
				System.exit(0);
			}
		}

		if(gap >= mInterval || gap <= -mInterval){
			seekTo(wantedPos);
			return mInterval + mLatencySeekTo;
		}

		if(gap <= minimumGap && gap >= -minimumGap){
			return 2000;
		}

		if(mMediaPlayer != null && mPrepared && trkId == mTrackerID){
			float newSpeed = 1.0f + (float)gap / (float)(mInterval);
			long before = System.currentTimeMillis();

			if(newSpeed <= mMinSpeed){
				mMediaPlayer.pause();
			}else if(newSpeed > mMinSpeed && newSpeed < mMaxSpeed){
				try{
					mMediaPlayer.setPlaybackParams(mMediaPlayer.getPlaybackParams().setSpeed(newSpeed));
				}catch(Exception e){
					Log.d(TAG, "setPlaybackParams failed, speed: " + newSpeed);
				}
			}

			int latencyChangeSpeed = Long.valueOf(System.currentTimeMillis() - before).intValue() + dT;

			class SetSpeedNormal implements Runnable {
				@Override
				public void run() {
					if(TgmVideoView.this.mMediaPlayer != null && TgmVideoView.this.mPrepared && trkId == mTrackerID){

						if(TgmVideoView.this.mMediaPlayer.isPlaying()){
							try{
								TgmVideoView.this.mMediaPlayer.setPlaybackParams(TgmVideoView.this.mMediaPlayer.getPlaybackParams().setSpeed(1.0f));
							}catch(Exception e){
								Log.d(TAG, "setPlaybackParams failed, set to default speed.");
							}
						}else{
							TgmVideoView.this.mMediaPlayer.start();
						}
					}
				}
			}
			int interval = Math.max(mInterval - latencyChangeSpeed, 100);
			mHandler.postDelayed(new SetSpeedNormal(), interval);

			return mInterval + interval;
		}else{
			return 100;
		}
	}

	private synchronized void stopTracking(){
		Log.d(TAG, "stopTracking...");
		mIsTracking = false;
		mTrackerID = (mTrackerID + 1)%100;
	}

	private synchronized void startTracking() {
		if(mIsTracking) return;
		mIsTracking = true;
		mTrackerID = (mTrackerID + 1)%100;
		Log.d(TAG, "startTracking, with ID: " + mTrackerID);

		class TrackingRunnable implements Runnable {
			int trkId;
			TrackingRunnable(int id){
				trkId = id;
			}
			@Override
			public void run() {
				if(mMediaPlayer != null && mPrepared && trkId == mTrackerID){
					int delay = tuneVideoSpeed(trkId);
					if(delay > 0 && trkId == mTrackerID){
						mHandler.postDelayed(this, delay);
					}else{
						mIsTracking = false;
						Log.d(TAG, "stop tracker with tracker ID: " + trkId + " current ID: " + mTrackerID + " delay: " + delay);
					}
				}else{
					mIsTracking = false;
					Log.d(TAG, "stop tracker with tracker ID: " + trkId + " current ID: " + mTrackerID);
				}
			}
		}
		mHandler.postDelayed(new TrackingRunnable(mTrackerID), 1000);
	}

	public synchronized void newSeekTo(int msec, long startTimeMS, CallbackContext callbackContext) throws Exception{
		//if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
		if (mMediaPlayer != null && mPrepared) {
			mMediaPlayer.setOnSeekCompleteListener(mSeekCompleteListener);
			mStartTimeMS = startTimeMS;
			seekTo(msec);
			Log.d(TAG, "newSeekTo: " + msec + " startTimeMS: " + startTimeMS);
			//startTracking();
			callbackContext.success("newSeekTo: OK");

		}else{
			throw new Exception("seekTo in illegal state.");
		}
	}

	boolean mOnSeekCompleteListening = false;
	OnSeekCompleteListener mSeekCompleteListener = new OnSeekCompleteListener() {
		public void onSeekComplete(MediaPlayer mp) {
			//SystemClock.sleep(200);
			//TgmVideoView.this.mMediaPlayer.setPlaybackParams(mMediaPlayer.getPlaybackParams().setSpeed(1.0f));
			if(mOnSeekCompleteListening){
				Log.d(TAG, "onSeekComplete duplicated");
				return;
			}
			mOnSeekCompleteListening = true;

			long curTimeMS = System.currentTimeMillis();
			mLatencySeekTo = Math.min(Long.valueOf(curTimeMS - mTriggerTimeSeek).intValue(), 300);
			//Log.d(TAG, "onSeekComplete, latency: " + mLatencySeekTo);
			long waitingTime = mStartTimeMS - curTimeMS - mStartLatency;
			Log.d(TAG, "mStartTimeMS: " + mStartTimeMS + ", curTimeMS: " + curTimeMS + ", mStartLatency: " + mStartLatency + ", waitingTime: " + waitingTime);

			if(waitingTime > 0){
				class StartTracking implements Runnable {
					@Override
					public void run() {
						if(TgmVideoView.this.mMediaPlayer != null) TgmVideoView.this.mMediaPlayer.start();
						//TgmVideoView.this.startTracking();
						TgmVideoView.this.mOnSeekCompleteListening = false;
						show();
					}
				}
				hide();
				mHandler.postDelayed(new StartTracking(), waitingTime);
				Log.d(TAG, "onSeekComplete, waiting: " + waitingTime);
			}else{
				if(mMediaPlayer != null) mMediaPlayer.start();
				//startTracking();
				mOnSeekCompleteListening = false;
				show();
			}

			/*
			if(waitingTime > 0){
				//SystemClock.sleep(waitingTime);
				try{
					Thread.sleep(waitingTime);
					//wait(waitingTime);
				}catch(Exception e){
				}
			}
			if(TgmVideoView.this.mMediaPlayer != null) TgmVideoView.this.mMediaPlayer.start();
			
			startTracking();
			*/
		}
	};

	OnBufferingUpdateListener mBufferingUpdateListener = new OnBufferingUpdateListener() {
		public void onBufferingUpdate(MediaPlayer mp, int percent) {
			Log.d(TAG, "seek onBufferingUpdate: " + percent);
		}
	};

	OnCompletionListener mCompletionListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			Log.d(TAG, "onCompletion");
			//mIsTracking = false;
			//mTrackerID += 1;
			hide();
		}
	};

	OnErrorListener mErrorListener = new OnErrorListener() {
		public boolean onError(MediaPlayer mp, int what, int extra) {
			Log.d(TAG, "Event, Error: " + what + "," + extra);
			if (what == MediaPlayer.MEDIA_ERROR_UNKNOWN && extra == -12) {
				Log.d(TAG, "No memory available to decode video");
				//Log.d(TAG, "No memory available to decode video, restart this app");
				//SystemClock.sleep(5000);
				//mMediaPlayer.release();
				//System.exit(0);
			}
			mStartCallbackContext.error(TAG + ": Error: " + what + "," + extra);
			return true;
		}
	};

	OnInfoListener mInfoListener = new OnInfoListener() {
		public boolean onInfo(MediaPlayer mp, int what, int extra) {
			Log.d(TAG, "Event, Info: " + what + "," + extra);
			if(what == MediaPlayer.MEDIA_INFO_VIDEO_NOT_PLAYING){
				Log.w(TAG, "MEDIA_INFO_VIDEO_NOT_PLAYING reported, notify fail.");
				mStartCallbackContext.error(TAG + ", error, MEDIA_INFO_VIDEO_NOT_PLAYING");
			}else if(what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START){
				Log.d(TAG, "MEDIA_INFO_VIDEO_RENDERING_START reported, notify success.");
				//mMediaPlayer.pause();
				mStartCallbackContext.success(TAG + ", MEDIA_INFO_VIDEO_RENDERING_START");
			}else{
				Log.d(TAG, "MEDIA_INFO reported, ignore it: " + what);
			}
			return true;
		}
	};

	public void changeVideoSource(String url, CallbackContext callbackContext) {
		try {
			if(mMediaPlayer.isPlaying()){
				mMediaPlayer.stop();
			}
			mMediaPlayer.reset();
			//release();
			//stopPlayback();
			mPrepared = false;
			initializeMediaPlayer(url, mScalableType, callbackContext);

			//if (mMediaPlayer != null && mSurface != null) {
			//	Log.d(TAG, "SurfaceTexture attached to changed MediaPlayer");
			//	mMediaPlayer.setSurface(mSurface);
			//}
			//callbackContext.success('changeVideoSource success');
		} catch (Exception e) {
			callbackContext.error(e.getMessage());
		}
	}

	public void hide(){
		setVisibility(View.INVISIBLE);
		if(mMediaPlayer.isPlaying()){
			Log.d(TAG, "in hide, pause");
			mMediaPlayer.pause();
		}
		stopTracking();
		setVolume(0, 0);
	}

	public void show(){
		startTracking();
		setVisibility(View.VISIBLE);
		setVolume(1.0f, 1.0f);
	}
}
