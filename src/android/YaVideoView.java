/**
 * Copyright (C) 2016 Zidoo (www.zidoo.tv)
 * Created by : jiangbo@zidoo.tv
 */

package com.tgm.cordova.videoplayer;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.os.Handler;

public class YaVideoView extends SurfaceView {
	protected static final String LOG_TAG = "YaVideoView";
	private Uri							mUri						= null;
	private String						mPath						= null;
	private SurfaceHolder				mSurfaceHolder				= null;
	public MediaPlayer					mMediaPlayer				= null;
	private OnPreparedListener			mOnPreparedListener			= null;
	private OnErrorListener				mOnErrorListener			= null;
	private OnInfoListener				mOnInfoListener				= null;
	private OnBufferingUpdateListener	mOnBufferingUpdateListener	= null;
	private OnCompletionListener		mCompletionListener			= null;
	private OnSeekCompleteListener		mSeekCompleteListener		= null;
	private Context						mContext					= null;
	private boolean						mIsRealtimeMedia			= false;

	private VideoDecodeThread		mVideoDecodeThread	= null;
	private Handler					mHandler				= new Handler();


	public YaVideoView(Context context) {
		super(context);
		initVideoView(context);
	}

	public YaVideoView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		initVideoView(context);
	}

	public YaVideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initVideoView(context);
	}

	private void initVideoView(Context ctx) {
		mContext = ctx;
		getHolder().addCallback(mSHCallback);
		requestFocus();
		if (ctx instanceof Activity)
			((Activity) ctx).setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	/*
	public void setVideoUrl(String url) {
		try {
			mIsRealtimeMedia = url.startsWith("rtsp:") || url.startsWith("rtmp:") || url.endsWith(".m3u8") || url.endsWith(".mpd");
			mUri = Uri.parse(url);
			openVideo();
		} catch (Exception e) {
		}
	}
	*/

	public void setVideoURI(Uri uri) {
		try {
			mUri = uri;
			String url = mUri.toString();
			mIsRealtimeMedia = url.startsWith("rtsp:") || url.startsWith("rtmp:") || url.endsWith(".m3u8") || url.endsWith(".mpd");
			//mUri = Uri.parse(url);
			//Log.d(LOG_TAG, url => );
			//openVideo();
		} catch (Exception e) {
		}
	}

	public synchronized void stopPlayback() {
		if (mMediaPlayer != null) {
			if(mMediaPlayer.isPlaying()){
				mMediaPlayer.pause();
				mMediaPlayer.stop();
			}
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		stopVCodec();
	}

	public void seekTo(long seek) {
		if (mMediaPlayer != null) {
			mMediaPlayer.seekTo((int) seek);
		}
	}

	public void start() {
		if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
			mMediaPlayer.start();

			startVCodec();
		}
	}

	public void pause() {
		if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
			mMediaPlayer.pause();
		}
	}

	private void changePlayer(){
		try {
			Method useRTMediaPlayer=MediaPlayer.class.getDeclaredMethod("useRTMediaPlayer", Integer.TYPE);
			useRTMediaPlayer.setAccessible(true);
			useRTMediaPlayer.invoke(mMediaPlayer, 2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		if (am.isSpeakerphoneOn()) {
			am.setSpeakerphoneOn(false);
		}
		am.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
		am.setStreamVolume(AudioManager.STREAM_VOICE_CALL,0, AudioManager.FLAG_PLAY_SOUND); 
		//mMediaPlayer.setAudioStreamType(streamtype);
		*/
	}

	private void openVideo() {
		if (mUri == null || mSurfaceHolder == null)
			return;
		// Intent i = new Intent("com.android.music.musicservicecommand");
		// i.putExtra("command", "pause");
		// mContext.sendBroadcast(i);
		try {

			if(mMediaPlayer != null) stopPlayback();

			mMediaPlayer = new MediaPlayer();

			/* TODO, for realtek 
			if(!mIsRealtimeMedia){
				changePlayer();
			}

			/*
			mMediaPlayer.setOnPreparedListener(mPreparedListener);
			mMediaPlayer.setOnErrorListener(mOnErrorListener);
			mMediaPlayer.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
			mMediaPlayer.setOnInfoListener(mOnInfoListener);
			mMediaPlayer.setDataSource(mContext, mUri);
			//mMediaPlayer.setDisplay(mSurfaceHolder);
			mMediaPlayer.setOnCompletionListener(mCompletionListener);
			mMediaPlayer.setOnSeekCompleteListener(mSeekCompleteListener);
			mMediaPlayer.setOnVideoSizeChangedListener(onVideoSizeChangedListener);
			mMediaPlayer.setScreenOnWhilePlaying(true);
			mMediaPlayer.prepareAsync();
			*/

			mMediaPlayer.setOnPreparedListener(mPreparedListener);
			mMediaPlayer.setOnErrorListener(mOnErrorListener);
			mMediaPlayer.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
			mMediaPlayer.setOnInfoListener(mOnInfoListener);
			mMediaPlayer.setDataSource(mContext, mUri);
			mMediaPlayer.setOnCompletionListener(mCompletionListener);
			mMediaPlayer.setOnSeekCompleteListener(mSeekCompleteListener);
			mMediaPlayer.prepareAsync();


		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	OnPreparedListener	mPreparedListener	= new OnPreparedListener() {
		public void onPrepared(MediaPlayer mp) {
			mMediaPlayer.start();
			startVCodec();
			if (mOnPreparedListener != null)
				mOnPreparedListener.onPrepared(mMediaPlayer);
		}
	};

	public void setOnSeekCompleteListener(OnSeekCompleteListener l) {
		mSeekCompleteListener = l;
	}

	public void setOnCompletionListener(OnCompletionListener l) {
		mCompletionListener = l;
	}

	public void setOnPreparedListener(OnPreparedListener l) {
		mOnPreparedListener = l;
	}

	public void setOnErrorListener(OnErrorListener l) {
		mOnErrorListener = l;
	}

	public void setOnBufferingUpdateListener(OnBufferingUpdateListener l) {
		mOnBufferingUpdateListener = l;
	}

	public void setOnInfoListener(OnInfoListener l) {
		mOnInfoListener = l;
	}

	SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback() {
		public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
			// holder.setFixedSize(w,
			// h);
		}

		public void surfaceCreated(SurfaceHolder holder) {
			mSurfaceHolder = holder;
			if (mMediaPlayer != null) {
				//mMediaPlayer.setDisplay(mSurfaceHolder);
			} else {
				if (mUri != null) {
					openVideo();
				}
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			mSurfaceHolder = null;
		}
	};

	OnVideoSizeChangedListener onVideoSizeChangedListener = new OnVideoSizeChangedListener() {
		public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
		}
	};


	private void startVCodec(){
		Log.v(LOG_TAG, "play");
		if (mVideoDecodeThread == null || !mVideoDecodeThread.isAlive()) {

			String url = mUri.toString();
			mVideoDecodeThread = new VideoDecodeThread(mSurfaceHolder.getSurface(), url, 0, -1, new VideoDecodeThread.PlayCompleteListener() {
				@Override
				public void playComplete() {
					Log.v(LOG_TAG, "complete");

					/*
					mHandler.postDelayed(new Runnable() {
						@Override
						public void run() {
							stopPlayback();
							//dialog.dismiss();
						}
					}, 100);
					*/
				}
			});
			mVideoDecodeThread.start();
		}

		while (!mVideoDecodeThread.isAlive()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}

	private void stopVCodec(){

		if (mVideoDecodeThread != null) {
			if (mVideoDecodeThread.isAlive()) {
				mVideoDecodeThread.stopThread();
			}
		}

		int timeout = 5;
		if (mVideoDecodeThread != null) {
			while (mVideoDecodeThread.isAlive()) {
				try {
					Thread.sleep(500);
					timeout--;
				} catch (InterruptedException ex) {
				}

				if (timeout < 0) {
					mVideoDecodeThread.releaseResource();
					break;
				}
			}
			//mVideoDecodeThread.releaseResource();
			mVideoDecodeThread = null;
		}
	}

}
