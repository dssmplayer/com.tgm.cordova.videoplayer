package com.tgm.cordova.videoplayer.rtspplayer;

import com.tgm.cordova.videoplayer.rtspplayer.rtsp.RtspClient;
import com.tgm.cordova.videoplayer.rtspplayer.rtsp.RtspClient.AudioTrack;
import com.tgm.cordova.videoplayer.rtspplayer.rtsp.RtspClient.Builder;
import com.tgm.cordova.videoplayer.rtspplayer.rtsp.RtspClient.RtspClientListener;
import com.tgm.cordova.videoplayer.rtspplayer.rtsp.RtspClient.SdpInfo;
import com.tgm.cordova.videoplayer.rtspplayer.rtsp.RtspClient.VideoTrack;
import com.tgm.cordova.videoplayer.rtspplayer.decode.AudioDecodeThread;
import com.tgm.cordova.videoplayer.rtspplayer.decode.FrameQueue;
import com.tgm.cordova.videoplayer.rtspplayer.decode.VideoDecodeThread;
import com.tgm.cordova.videoplayer.rtspplayer.decode.FrameQueue.Frame;
import com.tgm.cordova.videoplayer.rtspplayer.utils.NetUtils;
import com.tgm.cordova.videoplayer.rtspplayer.utils.VideoCodecUtils;
import com.tgm.cordova.videoplayer.rtspplayer.utils.VideoCodecUtils.NalUnit;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import android.util.Log;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import java.io.IOException;

import androidx.annotation.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import android.view.Surface;

public class RtspPlayer{
	protected static final String TAG = "RtspPlayer";
	private static final int DEFAULT_RTSP_PORT = 554;
	private static final int DEBUG = 10;

	private RtspThread rtspThread;

	private AtomicBoolean rtspStopped = new AtomicBoolean(true);
	private VideoDecodeThread videoDecodeThread;
	private AudioDecodeThread audioDecodeThread;
	private FrameQueue videoFrameQueue = new FrameQueue();
	private FrameQueue audioFrameQueue = new FrameQueue();

	private Surface surface;
	//private int surfaceWidth = 1920;
	//private int surfaceHeight = 1080;
	private int surfaceWidth = 1280;
	private int surfaceHeight = 720;

	private String videoMimeType = "";
	private String audioMimeType = "";
	private int audioSampleRate;
	private int audioChannelCount;
	private byte[] audioCodecConfig;

	public long vqPushCount = 0L;

	public String urlRtsp = "";
	private RtspPlayerListener listener;

    public interface RtspPlayerListener {
        void onFailed();
        void onClose();
        void onSuccess();
	}

	public VideoDecodeThread getVideoDecodeThread(){
		return videoDecodeThread;
	}

	public void start(Surface surface, String url, int width, int height, RtspPlayerListener listener){
		if (DEBUG > 0) Log.d(TAG, "try to Start RTSP thread...");
		this.urlRtsp = url;
		this.surface = surface;
		this.surfaceWidth = width;
		this.surfaceHeight = height;
		this.listener = listener;

		if(this.rtspStopped.get()){
			if (DEBUG > 0) Log.d(TAG, "Starting RTSP thread...");
		
			this.rtspStopped.set(false);
			this.rtspThread = new RtspThread();
			if(this.rtspThread != null) this.rtspThread.start();
		}
	}

	public void stop(){
        if (DEBUG > 0) Log.d(TAG, "Stopping RTSP thread...");
		this.rtspStopped.set(false);
		this.onRtspClientStopped();
		if(this.rtspThread != null){
            rtspThread.interrupt();
		}
	}

	/*
	private void restart(){
        if (DEBUG > 0) Log.d(TAG, "restart");
		this.stop();
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run(){
				start(RtspPlayer.this.surface, RtspPlayer.this.urlRtsp, RtspPlayer.this.surfaceWidth, RtspPlayer.this.surfaceHeight);
			}
		}, 3000);
	}
	*/

	public final void onRtspClientStarted() {
		if (DEBUG > 0) Log.v(TAG, "onRtspClientStarted()");
		this.rtspStopped.set(false);
	}

	public final void onRtspClientStopped() {
		if (DEBUG > 0) Log.v(TAG, "onRtspClientStopped()");
		this.rtspStopped.set(true);
		
		if (this.videoDecodeThread != null) {
			this.videoDecodeThread.interrupt();
			this.videoDecodeThread = null;
		}

		if (this.audioDecodeThread != null) {
			this.audioDecodeThread.interrupt();
			this.audioDecodeThread = null;
		}

		this.vqPushCount = 0L;
		RtspPlayer.this.listener.onClose();
	}

	public final void onRtspClientConnected() {
        if (DEBUG > 0) Log.v(TAG, "onRtspClientConnected()");

		if(this.videoMimeType.length() > 0){
            Log.i(TAG, "Starting video decoder with mime type: " + this.videoMimeType);
            this.videoDecodeThread = new VideoDecodeThread(this.surface, this.videoMimeType, this.surfaceWidth, this.surfaceHeight, this.videoFrameQueue);
			if(this.videoDecodeThread != null) this.videoDecodeThread.start();
		}

		/*
		if(this.audioMimeType.length() > 0){
            Log.i(TAG, "Starting audio decoder with mime type: " + this.audioMimeType);
            this.audioDecodeThread = new AudioDecodeThread(this.audioMimeType, this.audioSampleRate, this.audioChannelCount, this.audioCodecConfig, this.audioFrameQueue);
			if(this.audioDecodeThread != null) this.audioDecodeThread.start();
		}
		*/

		RtspPlayer.this.listener.onSuccess();
    }

	public final class RtspThread extends Thread {
		public void run() {
			/*
			(new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
				public final void run() {
					RtspPlayer.this.onRtspClientStarted();
				}
			}));
			*/

			RtspClientListener listener = new RtspClientListener() {
				public void onRtspDisconnected() {
					if (DEBUG > 0) Log.v(TAG, "onRtspDisconnected()");
					RtspPlayer.this.rtspStopped.set(true);
				}

				public void onRtspFailed(@Nullable final String message) {
					if (DEBUG > 0) Log.e(TAG, "onRtspFailed(message=\"" + message + "\")");
					RtspPlayer.this.rtspStopped.set(true);
					//RtspPlayer.this.restart();
					RtspPlayer.this.listener.onFailed();
				}

				public void onRtspConnected(@NotNull final SdpInfo sdpInfo) {
					if (DEBUG > 0) Log.v(TAG, "onRtspConnected()");
					(new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
						public final void run() {
							String s = "";
							if (sdpInfo.videoTrack != null)
								s = "video";
							if (sdpInfo.audioTrack != null) {
								if (s.length() > 0)
									s = s + ", ";

								s = s + "audio";
							}
						}
					}));

                    if (sdpInfo.videoTrack != null) {
                        videoFrameQueue.clear();
						//if(sdpInfo.videoTrack.videoCodec == RtspClient.VIDEO_CODEC_H264) videoMimeType = "video/avc";
						//else if(sdpInfo.videoTrack.videoCodec == RtspClient.VIDEO_CODEC_H265) videoMimeType = "video/avc";
						if(sdpInfo.videoTrack.videoCodec == RtspClient.VIDEO_CODEC_H264) videoMimeType = "video/avc";
						else if(sdpInfo.videoTrack.videoCodec == RtspClient.VIDEO_CODEC_H265) videoMimeType = "video/hevc";

						if(sdpInfo.audioTrack != null){
							if(sdpInfo.audioTrack.audioCodec == RtspClient.AUDIO_CODEC_AAC) audioMimeType = "audio/mp4a-latm";
						}

						byte[] sps = sdpInfo.videoTrack.sps;
						byte[] pps = sdpInfo.videoTrack.pps;

                        if (sps != null && pps != null) {
							byte[] data = new byte[sps.length + pps.length];
							System.arraycopy(sps, 0, data, 0 , sps.length);
							System.arraycopy(pps, 0, data, sps.length, pps.length);

							try{
								RtspPlayer.this.videoFrameQueue.push(new Frame(data, 0, data.length, 0L));
							}catch(Exception e){
								e.printStackTrace();
							}
							vqPushCount++;

							if (DEBUG > 90 && videoDecodeThread != null && videoDecodeThread != null)
								Log.d(TAG, "count_info: " + vqPushCount + ',' + videoDecodeThread.vqPopCount + ',' + videoDecodeThread.renderCount);

                        } else {
                            if (DEBUG > 10) Log.d(TAG, "RTSP SPS and PPS NAL units missed in SDP");
                        }
					}

                    if (sdpInfo.audioTrack != null) {
                        audioFrameQueue.clear();

						if(sdpInfo.audioTrack.audioCodec == RtspClient.AUDIO_CODEC_AAC) audioMimeType = "audio/mp4a-latm";

                        audioSampleRate = sdpInfo.audioTrack.sampleRateHz;
                        audioChannelCount = sdpInfo.audioTrack.channels;
                        audioCodecConfig = sdpInfo.audioTrack.config;
                    }
					RtspPlayer.this.onRtspClientConnected();
                }

				public void onRtspFailedUnauthorized() {
					if (DEBUG > 0) Log.e(TAG, "onRtspFailedUnauthorized()");
					RtspPlayer.this.rtspStopped.set(true);
				}

				public void onRtspVideoNalUnitReceived(@NotNull byte[] data, int offset, int length, long timestamp) throws IOException {
					if (DEBUG > 90) Log.v(TAG, "onRtspVideoNalUnitReceived(length=" + length + ", timestamp=" + timestamp + ')');

					if (length > 0) {
						try{
							boolean result = RtspPlayer.this.videoFrameQueue.push(new Frame(data, offset, length, timestamp));
							if(!result){
								Log.e(TAG, "Error:: onRtspVideoNalUnitReceived(length=" + length + ", timestamp=" + timestamp + ')');
								throw new Exception("Fail to push rtsp video frame to queue");
								//Log.w(TAG, "Fail to push rtsp video frame to queue");
							}
						}catch(Exception e){
							e.printStackTrace();
							throw new IOException("Throw failure to push rtsp video frame to queue");
						}
						vqPushCount++;
						if (DEBUG > 90 && videoDecodeThread != null && videoDecodeThread != null)
							Log.d(TAG, "count_info__: " + vqPushCount + ',' + videoDecodeThread.vqPopCount + ',' + videoDecodeThread.renderCount);
					}

				}

				public void onRtspAudioSampleReceived(@NotNull byte[] data, int offset, int length, long timestamp) throws IOException {
					if (DEBUG > 90) Log.v(TAG, "onRtspAudioSampleReceived(length=" + length + ", timestamp=" + timestamp + ')');
					if (length > 0) {
						try{
							boolean result = RtspPlayer.this.audioFrameQueue.push(new Frame(data, offset, length, timestamp));
							if(!result){
								throw new Exception("Fail to push rtsp audio frame to queue");
							}
						}catch(Exception e){
							e.printStackTrace();
							throw new IOException("Throw failure to push rtsp audio frame to queue");
						}
					}
				}

				public void onRtspConnecting() {
					if (DEBUG > 0) Log.v(TAG, "onRtspConnecting()");
				}
			};

			Uri uri = Uri.parse(RtspPlayer.this.urlRtsp);
			int port = uri.getPort() == -1 ? 554 : uri.getPort();

			String userInfoString = uri.getUserInfo();
			String username = "";
			String password = "";

			if(userInfoString != null){
				final String[] userInfo = userInfoString.split(":");
				username = userInfo[0];
				if(userInfo.length > 1){
					password = userInfo[1];
				}
			}

			try {
				if (DEBUG > 0) Log.d(TAG, "Connecting to " + uri.getHost() + ':' + port + "...");
				Socket socket = NetUtils.createSocketAndConnect(String.valueOf(uri.getHost()), port, 5000);
				Builder rtspBuilder = new Builder(socket, uri.toString(), RtspPlayer.this.rtspStopped, (RtspClientListener)listener);
				RtspClient rtspClient = rtspBuilder
					.requestVideo(true)
					.requestAudio(false)
					.withDebug(true)
					.withUserAgent("ConnectPlayer RTSP Client")
					.withCredentials(username, password)
					.build();

				rtspClient.execute();

				NetUtils.closeSocket(socket);
			} catch (Exception e) {
				e.printStackTrace();
			}

			(new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
				public final void run() {
					RtspPlayer.this.onRtspClientStopped();
				}
			}));
		}
	}
};




