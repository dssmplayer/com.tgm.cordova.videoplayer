package com.tgm.cordova.videoplayer.rtspplayer.decode;

import android.util.Log;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
//import kotlin.Metadata;
//import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/*
@Metadata(
   mv = {1, 4, 2},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0012B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\f\u001a\u00020\rJ\b\u0010\u000e\u001a\u0004\u0018\u00010\tJ\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\tR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0013"},
   d2 = {"Lcom/tgm/cordova/rtspplayer/decode/FrameQueue;", "", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "queue", "Ljava/util/concurrent/BlockingQueue;", "Lcom/tgm/cordova/rtspplayer/decode/FrameQueue$Frame;", "getQueue", "()Ljava/util/concurrent/BlockingQueue;", "clear", "", "pop", "push", "", "frame", "Frame", "rtsp-client-android.app"}
)
*/

public final class FrameQueue {
   @NotNull
   private final String TAG;
   @NotNull
   private final BlockingQueue queue;

   @NotNull
   public final String getTAG() {
      return this.TAG;
   }

   @NotNull
   public final BlockingQueue getQueue() {
      return this.queue;
   }

   public final boolean push(@NotNull FrameQueue.Frame frame) throws InterruptedException {
      //Intrinsics.checkNotNullParameter(frame, "frame");
	  if(frame == null) return false;
      if (this.queue.offer(frame, 5L, TimeUnit.MILLISECONDS)) {
         return true;
      } else {
         Log.w(this.TAG, "Cannot add frame, queue is full");
         return false;
      }
   }

   @Nullable
   public final FrameQueue.Frame pop() throws InterruptedException {
      try {
         FrameQueue.Frame frame = (FrameQueue.Frame)this.queue.poll(1000L, TimeUnit.MILLISECONDS);
         if (frame == null) {
            Log.w(this.TAG, "Cannot get frame, queue is empty");
         }

         return frame;
      } catch (InterruptedException var2) {
         Thread.currentThread().interrupt();
         return null;
      }
   }

   public final void clear() {
      this.queue.clear();
   }

   public FrameQueue() {
      //String var10001 = FrameQueue.class.getSimpleName();
      //Intrinsics.checkNotNullExpressionValue(var10001, "FrameQueue::class.java.simpleName");
      this.TAG = FrameQueue.class.getSimpleName();
      this.queue = (BlockingQueue)(new ArrayBlockingQueue(60));
   }

   /*
   @Metadata(
      mv = {1, 4, 2},
      bv = {1, 0, 3},
      k = 1,
      d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\t\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0011"},
      d2 = {"Lcom/tgm/cordova/rtspplayer/decode/FrameQueue$Frame;", "", "data", "", "offset", "", "length", "timestamp", "", "([BIIJ)V", "getData", "()[B", "getLength", "()I", "getOffset", "getTimestamp", "()J", "rtsp-client-android.app"}
   )
   */
   public static final class Frame {
      @NotNull
      private final byte[] data;
      private final int offset;
      private final int length;
      private final long timestamp;

      @NotNull
      public final byte[] getData() {
         return this.data;
      }

      public final int getOffset() {
         return this.offset;
      }

      public final int getLength() {
         return this.length;
      }

      public final long getTimestamp() {
         return this.timestamp;
      }

      public Frame(@NotNull byte[] data, int offset, int length, long timestamp) {
         //Intrinsics.checkNotNullParameter(data, "data");
         super();
         this.data = data;
         this.offset = offset;
         this.length = length;
         this.timestamp = timestamp;
      }
   }
}


