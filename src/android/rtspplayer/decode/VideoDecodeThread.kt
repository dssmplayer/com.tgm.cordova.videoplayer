package com.tgm.cordova.rtspplayer.decode;

import android.media.MediaCodec
import android.media.MediaFormat
import android.util.Log
import android.view.Surface
import java.nio.ByteBuffer

class VideoDecodeThread (
        private val surface: Surface,
        private val mimeType: String,
        private val width: Int,
        private val height: Int,
        private val videoFrameQueue: FrameQueue) : Thread() {

    private val TAG: String = VideoDecodeThread::class.java.simpleName
    private val DEBUG = true
	public var vqPopCount = 0L;
	public var renderCount = 0L;

    override fun run() {
        if (DEBUG) Log.d(TAG, "VideoDecodeThread started")

        val decoder = MediaCodec.createDecoderByType(mimeType)
        val format = MediaFormat.createVideoFormat(mimeType, width, height)

        decoder.configure(format, surface, null, 0)
        decoder.start()

        val bufferInfo = MediaCodec.BufferInfo()
		
		var startTime = 0L;
		var currentTime = 0L;

        while (!interrupted()) {
            val inIndex: Int = decoder.dequeueInputBuffer(10000L)
            //val inIndex: Int = decoder.dequeueInputBuffer(5)
            if (inIndex >= 0) {
                // fill inputBuffers[inputBufferIndex] with valid data
                val byteBuffer: ByteBuffer? = decoder.getInputBuffer(inIndex)
                byteBuffer?.rewind()

                // Preventing BufferOverflowException
//              if (length > byteBuffer.limit()) throw DecoderFatalException("Error")

                val frame: FrameQueue.Frame?
                try {
                    frame = videoFrameQueue.pop()
                    if (frame == null) {
                        Log.d(TAG, "Empty frame")
                    } else {

						vqPopCount++;

						if(startTime == 0L) startTime = System.currentTimeMillis();

                        byteBuffer!!.put(frame.data, frame.offset, frame.length)
                        decoder.queueInputBuffer(inIndex, frame.offset, frame.length, frame.timestamp, 0)
                        //decoder.queueInputBuffer(inIndex, frame.offset, frame.length, System.currentTimeMillis() * 1000 + 5000000, 0)
                        //decoder.queueInputBuffer(inIndex, frame.offset, frame.length, 0, 0)
                        //Log.d(TAG, "timestamp: " + frame.timestamp)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            try {
                val outIndex = decoder.dequeueOutputBuffer(bufferInfo, 10000)
                //val outIndex = decoder.dequeueOutputBuffer(bufferInfo, 0)
                //Log.d(TAG, "outIndex: " + outIndex)
                when (outIndex) {
                    MediaCodec.INFO_OUTPUT_FORMAT_CHANGED -> Log.d(TAG, "Decoder format changed: " + decoder.outputFormat)
                    //MediaCodec.INFO_TRY_AGAIN_LATER -> if (DEBUG) Log.d(TAG, "No output from decoder available")
                    else -> {
                        if (outIndex >= 0) {

							if(currentTime == 0L){
								currentTime = System.currentTimeMillis();
								Log.d(TAG, "Playing Time Latency: " + currentTime)
								Log.d(TAG, "Playing Time Latency: " + startTime)
								Log.d(TAG, "Playing Time Latency: " + (currentTime - startTime))
							}

                            //decoder.releaseOutputBuffer(outIndex, bufferInfo.size != 0)
                            decoder.releaseOutputBuffer(outIndex, true)
							renderCount++;
                        }
                    }
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            // All decoded frames have been rendered, we can stop playing now
            if (bufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM != 0) {
                Log.d(TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM")
                break
            }
        }

        decoder.stop()
        decoder.release()
        videoFrameQueue.clear()

        if (DEBUG) Log.d(TAG, "VideoDecodeThread stopped")
    }
}

