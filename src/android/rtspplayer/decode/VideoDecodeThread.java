package com.tgm.cordova.videoplayer.rtspplayer.decode;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.media.MediaCodec.BufferInfo;
import android.util.Log;
import android.view.Surface;
import com.tgm.cordova.videoplayer.rtspplayer.decode.FrameQueue.Frame;
import java.nio.ByteBuffer;
//import kotlin.Metadata;
//import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;

import android.graphics.YuvImage;
import java.io.ByteArrayOutputStream;
import android.graphics.Rect;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import android.graphics.ImageFormat;
import android.util.Base64;
import android.os.Handler;
import android.media.Image;
import android.view.SurfaceView;
import java.lang.Thread;
import static java.lang.Math.min;
import static java.lang.Math.max;

import org.apache.cordova.CallbackContext;

import java.util.Map;

/*
@Metadata(
   mv = {1, 4, 2},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\b\u0010\u0018\u001a\u00020\u0019H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u00020\u0010X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u0010X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0012\"\u0004\b\u0017\u0010\u0014R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"},
   d2 = {"Lcom/tgm/cordova/rtspplayer/decode/VideoDecodeThread;", "Ljava/lang/Thread;", "surface", "Landroid/view/Surface;", "mimeType", "", "width", "", "height", "videoFrameQueue", "Lcom/tgm/cordova/rtspplayer/decode/FrameQueue;", "(Landroid/view/Surface;Ljava/lang/String;IILcom/tgm/cordova/rtspplayer/decode/FrameQueue;)V", "DEBUG", "", "TAG", "renderCount", "", "getRenderCount", "()J", "setRenderCount", "(J)V", "vqPopCount", "getVqPopCount", "setVqPopCount", "run", "", "rtsp-client-android.app"}
)
*/

public final class VideoDecodeThread extends Thread {
   private final String TAG;
   private final boolean DEBUG;
   public long vqPopCount;
   public long renderCount = 0;
   private final Surface surface;
   private final String mimeType;
   private final int width;
   private final int height;
   private final FrameQueue videoFrameQueue;
   private Handler handler = null;
   private SurfaceView surfaceView = null;

   private CallbackContext callbackContext = null;
   private boolean startTakeSnapshot = false;

   public final long getVqPopCount() {
      return this.vqPopCount;
   }

   public final void setVqPopCount(long var1) {
      this.vqPopCount = var1;
   }

   public final long getRenderCount() {
      return this.renderCount;
   }

   public final void setRenderCount(long var1) {
      this.renderCount = var1;
   }

   private final int getSafeWidth(int width){
	   return 1280;
   }

   private final int getSafeHeight(int height){
	   return 720;
   }

   public void run() {
      if (this.DEBUG) {
         Log.d(this.TAG, "VideoDecodeThread started");
      }

	  /*
      MediaCodec var10000 = MediaCodec.createDecoderByType(this.mimeType);
      Intrinsics.checkNotNullExpressionValue(var10000, "MediaCodec.createDecoderByType(mimeType)");
      MediaCodec decoder = var10000;
      MediaFormat var16 = MediaFormat.createVideoFormat(this.mimeType, this.width, this.height);
      Intrinsics.checkNotNullExpressionValue(var16, "MediaFormat.createVideoF…(mimeType, width, height)");
      MediaFormat format = var16;
	  */

	  this.renderCount = 0;
	  MediaCodec decoder = null;
	  MediaFormat format = null;
	  try{
		  decoder = MediaCodec.createDecoderByType(this.mimeType);
		  format = MediaFormat.createVideoFormat(this.mimeType, this.width, this.height);
	  }catch(Exception e){
		  Log.e(this.TAG, "cannot init decoder or format");
		  return;
	  }

	  try{
		Log.d(this.TAG, "decoder.configure() with mime: " + this.mimeType
				+ ", width: " + this.width + ", height: " + this.height
				+ ", decoder: " + decoder
				+ ", format: " + format
				+ ", surface:" + this.surface
				+ ", suface.isValid(): " + this.surface.isValid());
		decoder.configure((MediaFormat)format, (Surface)this.surface, (MediaCrypto)null, (int)0);
		decoder.start();
	  }catch(Exception e){
		Log.e(TAG, "decoder.configure() or decoder.start() failure: " + e + ":" + e.getMessage());
		e.printStackTrace();
		try{
		    decoder.release();
		  }catch(Exception e2){
			Log.e(this.TAG, "add by crow: cannot release decoder, in exception catcher");
		}
		return;
	  }

      BufferInfo bufferInfo = new BufferInfo();
      long startTime = 0L;
      long currentTime = 0L;

      while(!Thread.interrupted()) {
         int inIndex = decoder.dequeueInputBuffer(10000L);
         if (inIndex >= 0) {
            ByteBuffer byteBuffer = decoder.getInputBuffer(inIndex);
            if (byteBuffer != null) {
               byteBuffer.rewind();
            }

            Frame frame = null;

            try {
               frame = this.videoFrameQueue.pop();
               if (frame == null) {
                  Log.d(this.TAG, "Empty frame");
               } else {
                  this.vqPopCount++;
                  if (startTime == 0L) {
                     startTime = System.currentTimeMillis();
                  }

                  //Intrinsics.checkNotNull(byteBuffer);
				  if(byteBuffer == null){
				      Log.e(this.TAG, "KOTLIN: null bytebuffer");
					  return;
				  }

                  byteBuffer.put(frame.getData(), frame.getOffset(), frame.getLength());
                  decoder.queueInputBuffer(inIndex, frame.getOffset(), frame.getLength(), frame.getTimestamp(), 0);
               }
            } catch (Exception e) {
               e.printStackTrace();
            }
         }

         try {
            int outIndex = decoder.dequeueOutputBuffer(bufferInfo, 10000L);
            switch(outIndex) {
            case -2:
               Log.d(this.TAG, "Decoder format changed: " + decoder.getOutputFormat());
               break;
            default:
               if (outIndex >= 0) {
				  if(this.startTakeSnapshot == true){
					 this.startTakeSnapshot = false;
					 //saveBufferToBitmap(decoder, outIndex, bufferInfo);
					 CallbackContext cb = this.callbackContext;
					 pushCurrentPictureInBase64(decoder, outIndex, bufferInfo, cb);
					 //pushCurrentPictureInBase64_2(decoder, outIndex, bufferInfo, cb);
				  }

                  if (currentTime == 0L) {
                     currentTime = System.currentTimeMillis();
                     Log.d(this.TAG, "Playing Time Latency: " + currentTime);
                     Log.d(this.TAG, "Playing Time Latency: " + startTime);
                     Log.d(this.TAG, "Playing Time Latency: " + (currentTime - startTime));
                  }

                  decoder.releaseOutputBuffer(outIndex, true);
                  this.renderCount++;
               }
			   break;
            }
         } catch (Exception e) {
            e.printStackTrace();
         }

         if ((bufferInfo.flags & 4) != 0) {
            Log.d(this.TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
            break;
         }
      }

	  try{
		decoder.stop();
	  }catch(Exception e){
		Log.e(this.TAG, "add by crow: cannot stop decoder");
	  }
	  try{
        decoder.release();
	  }catch(Exception e){
		Log.e(this.TAG, "add by crow: cannot release decoder");
	  }
      this.videoFrameQueue.clear();
      if (this.DEBUG) {
         Log.d(this.TAG, "VideoDecodeThread stopped");
      }
   }

   //public void tryToGetCurrentPicture(CallbackContext callbackContext, Handler handler, SurfaceView surfaceView){
   public void tryToGetCurrentPicture(CallbackContext callbackContext){
       Log.d(this.TAG, "set callbackContext");
	   this.callbackContext = callbackContext;
	   this.startTakeSnapshot = true;
	   //this.handler = handler;
	   //this.surfaceView = surfaceView;
   }

	private void saveBitmap(Bitmap bitmap){
		if(bitmap != null){
			File imgFile = new File("/data/user/0/com.tgm.ndspplayer/cache/", "from.rtsp.jpg");
			try{
				imgFile.createNewFile();
				FileOutputStream out = new FileOutputStream(imgFile);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
				out.close();
			}catch(FileNotFoundException e){
				Log.e(TAG, "FileNotFoundException : " + e.getMessage());
			}catch(IOException e){
				Log.e(TAG, "IOException : " + e.getMessage());
			}
			Log.d(TAG, "save to: " + "/data/user/0/com.tgm.ndspplayer/cache/from.rtsp.jpg");
		}
	}

   private void __pushCurrentPictureInBase64(final byte[] ba, final int width, final int height, final CallbackContext cb){
   	   Log.v(TAG, "ba = " + ba.length + "");

	   YuvImage yuvimage = new YuvImage(ba, ImageFormat.NV21, width, height, null);
	   ByteArrayOutputStream baos = new ByteArrayOutputStream();
	   yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
	   byte[] jdata = baos.toByteArray();

	   String encodedImage = Base64.encodeToString(jdata, Base64.DEFAULT);
	   //Log.v(TAG, "encoded base64 image: " + encodedImage);
	   cb.success(encodedImage);

	   //Bitmap bitmap = BitmapFactory.decodeByteArray(jdata, 0, jdata.length);
	   //saveBitmap(bitmap);

   }

	private void pushCurrentPictureInBase64(final MediaCodec decoder, final int outIndex, final BufferInfo info, final CallbackContext cb){
		//Log.v(TAG, "start post new handler");
		ByteBuffer buffer = decoder.getOutputBuffers()[outIndex];
		//ByteBuffer buffer = decoder.getOutputBuffer(outIndex);

		buffer.position(info.offset);
		buffer.limit(info.offset + info.size);

		Log.v(TAG, "info: " + outIndex + "|" + info.offset + "|" + info.size + "|" + buffer.remaining() + "|");
		byte[] ba = new byte[buffer.remaining()];
		buffer.get(ba);
		Log.v(TAG, "ba: " + ba[0] + "|" + ba[1] + "|" + ba[2] + "|" + ba[3] + "|" + ba[4] + "|" + ba[5] + "|" + ba[6] + "|" + ba[7] + "|" + ba[8] + "|" + ba[9] + "|");

		int width = this.width;
		int height = this.height;

		new Thread(new Runnable() {
			@Override public void run() {
				__pushCurrentPictureInBase64(ba, width, height, cb);
			}
		}).start();

		//__pushCurrentPictureInBase64(ba, width, height, cb);
	}

   private static byte[] YUV_420_888toNV21(Image image) {
	   byte[] nv21;
	   ByteBuffer yBuffer = image.getPlanes()[0].getBuffer();
	   ByteBuffer uBuffer = image.getPlanes()[1].getBuffer();
	   ByteBuffer vBuffer = image.getPlanes()[2].getBuffer();
	   int ySize = yBuffer.remaining();
	   int uSize = uBuffer.remaining();
	   int vSize = vBuffer.remaining();
	   nv21 = new byte[ySize + uSize + vSize];
	   //U and V are swapped
	   yBuffer.get(nv21, 0, ySize);
	   vBuffer.get(nv21, ySize, vSize);
	   uBuffer.get(nv21, ySize + vSize, uSize);
	   return nv21;
   }

   private void pushCurrentPictureInBase64_2(final MediaCodec decoder, final int outIndex, final BufferInfo info, final CallbackContext cb){
	   Log.v(TAG, "start post new handler");
	   Image image = decoder.getOutputImage(outIndex);
	   YuvImage yuvImage = new YuvImage(YUV_420_888toNV21(image), ImageFormat.NV21, this.width, this.height, null);
	   ByteArrayOutputStream baos = new ByteArrayOutputStream();
	   yuvImage.compressToJpeg(new Rect(0, 0, this.width, this.height), 80, baos);
	   byte[] jdata = baos.toByteArray();

	   String encodedImage = Base64.encodeToString(jdata, Base64.DEFAULT);
	   cb.success(encodedImage);
   }

   private void pushCurrentPictureInBase64_3(final MediaCodec decoder, final int outIndex, final BufferInfo info, final CallbackContext cb){
	   Log.v(TAG, "start post new handler");

	   ByteBuffer buffer = decoder.getOutputBuffer(outIndex);
	   buffer.position(info.offset);
	   buffer.limit(info.offset + info.size);
	   byte[] ba = new byte[buffer.remaining()];
	   buffer.get(ba);

	   Image image = decoder.getOutputImage(outIndex);
	   YuvImage yuvImage = new YuvImage(YUV_420_888toNV21(image), ImageFormat.NV21, this.width, this.height, null);
	   ByteArrayOutputStream baos = new ByteArrayOutputStream();
	   yuvImage.compressToJpeg(new Rect(0, 0, this.width, this.height), 80, baos);
	   byte[] jdata = baos.toByteArray();

	   String encodedImage = Base64.encodeToString(jdata, Base64.DEFAULT);
	   cb.success(encodedImage);
   }

   public VideoDecodeThread(@NotNull Surface surface, @NotNull String mimeType, int width, int height, @NotNull FrameQueue videoFrameQueue) {
      //Intrinsics.checkNotNullParameter(surface, "surface");
      //Intrinsics.checkNotNullParameter(mimeType, "mimeType");
      //Intrinsics.checkNotNullParameter(videoFrameQueue, "videoFrameQueue");

      super();

      //String var10001 = VideoDecodeThread.class.getSimpleName();
      //Intrinsics.checkNotNullExpressionValue(var10001, "VideoDecodeThread::class.java.simpleName");
      //this.TAG = var10001;
	  this.TAG = VideoDecodeThread.class.getSimpleName();
      this.DEBUG = true;

	  if(surface == null || mimeType == null || videoFrameQueue == null){
		  Log.e(this.TAG, "KOTLIN: null surface, mimeType, videoFrameQueue");
	  }

      this.surface = surface;
      this.mimeType = mimeType;
      this.width = getSafeWidth(width);
      this.height = getSafeHeight(height);
      this.videoFrameQueue = videoFrameQueue;
   }
}


