package com.tgm.cordova.videoplayer.rtspplayer.decode;

import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.media.AudioAttributes.Builder;
import android.media.MediaCodec.BufferInfo;
import android.util.Log;
import android.view.Surface;
import com.tgm.cordova.videoplayer.rtspplayer.decode.FrameQueue.Frame;
import java.nio.ByteBuffer;
//import kotlin.Metadata;
//import kotlin.jvm.internal.DefaultConstructorMarker;
//import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/*
@Metadata(
   mv = {1, 4, 2},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\b\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0082D¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"},
   d2 = {"Lcom/tgm/cordova/rtspplayer/decode/AudioDecodeThread;", "Ljava/lang/Thread;", "mimeType", "", "sampleRate", "", "channelCount", "codecConfig", "", "audioFrameQueue", "Lcom/tgm/cordova/rtspplayer/decode/FrameQueue;", "(Ljava/lang/String;II[BLcom/tgm/cordova/rtspplayer/decode/FrameQueue;)V", "DEBUG", "", "TAG", "run", "", "Companion", "rtsp-client-android.app"}
)
*/
public final class AudioDecodeThread extends Thread {
   private final String TAG;
   private final boolean DEBUG;
   private final String mimeType;
   private final int sampleRate;
   private final int channelCount;
   private final byte[] codecConfig;
   private final FrameQueue audioFrameQueue;
   @NotNull
   //public static final AudioDecodeThread.Companion Companion = new AudioDecodeThread.Companion((DefaultConstructorMarker)null);
   public static final AudioDecodeThread.Companion Companion = new AudioDecodeThread.Companion();

   public void run() {
      if (this.DEBUG) {
         Log.d(this.TAG, "AudioDecodeThread started");
      }

	  /*
      MediaCodec var10000 = MediaCodec.createDecoderByType(this.mimeType);
      Intrinsics.checkNotNullExpressionValue(var10000, "MediaCodec.createDecoderByType(mimeType)");
      MediaCodec decoder = var10000;
      MediaFormat var22 = MediaFormat.createAudioFormat(this.mimeType, this.sampleRate, this.channelCount);
      Intrinsics.checkNotNullExpressionValue(var22, "MediaFormat.createAudioF…sampleRate, channelCount)");
      MediaFormat format = var22;
	  */

	  MediaCodec decoder = null;
	  MediaFormat format = null;
	  try{
		decoder = MediaCodec.createDecoderByType(this.mimeType);
		format = MediaFormat.createAudioFormat(this.mimeType, this.sampleRate, this.channelCount);
	  }catch(Exception e){
		  Log.e(this.TAG, "KOTLIN: cannot init decoder or format");
		  return;
	  }

      byte[] var23 = this.codecConfig;
      if (var23 == null) {
         var23 = Companion.getAacDecoderConfigData(2, this.sampleRate, this.channelCount);
      }

      byte[] csd0 = var23;
      ByteBuffer bb = ByteBuffer.wrap(csd0);
      format.setByteBuffer("csd-0", bb);
      format.setInteger("aac-profile", 2);
      decoder.configure(format, (Surface)null, (MediaCrypto)null, 0);
      decoder.start();
      int outChannel = this.channelCount > 1 ? 12 : 4;
      int outAudio = 2;
      int bufferSize = AudioTrack.getMinBufferSize(this.sampleRate, outChannel, outAudio);
      AudioTrack audioTrack = new AudioTrack((new Builder()).setUsage(1).setContentType(1).build(), (new android.media.AudioFormat.Builder()).setEncoding(outAudio).setChannelMask(outChannel).setSampleRate(this.sampleRate).build(), bufferSize, 1, 0);
      audioTrack.play();
      BufferInfo bufferInfo = new BufferInfo();

      while(!Thread.interrupted()) {
         int inIndex = decoder.dequeueInputBuffer(10000L);
         if (inIndex >= 0) {
            ByteBuffer byteBuffer = decoder.getInputBuffer(inIndex);
            if (byteBuffer != null) {
               byteBuffer.rewind();
            }

            Frame audioFrame = null;

            try {
               audioFrame = this.audioFrameQueue.pop();
               if (audioFrame == null) {
                  Log.d(this.TAG, "Empty frame");
               } else {
                  //Intrinsics.checkNotNull(byteBuffer);

				  if(byteBuffer == null){
				      Log.e(this.TAG, "KOTLIN: null bytebuffer");
					  return;
				  }

                  byteBuffer.put(audioFrame.getData(), audioFrame.getOffset(), audioFrame.getLength());
                  decoder.queueInputBuffer(inIndex, audioFrame.getOffset(), audioFrame.getLength(), audioFrame.getTimestamp(), 0);
               }
            } catch (Exception var19) {
               var19.printStackTrace();
            }
         }

         try {
            int outIndex = decoder.dequeueOutputBuffer(bufferInfo, 10000L);
            switch(outIndex) {
            case -2:
               Log.d(this.TAG, "Decoder format changed: " + decoder.getOutputFormat());
               break;
            case -1:
               if (this.DEBUG) {
                  Log.d(this.TAG, "No output from decoder available");
               }
               break;
            default:
               if (outIndex >= 0) {
                  ByteBuffer byteBuffer = decoder.getOutputBuffer(outIndex);
                  byte[] chunk = new byte[bufferInfo.size];
                  if (byteBuffer != null) {
                     byteBuffer.get(chunk);
                  }

                  if (byteBuffer != null) {
                     byteBuffer.clear();
                  }

                  boolean var15 = false;
                  boolean var17 = false;
                  if (chunk.length != 0) {
                     audioTrack.write(chunk, 0, chunk.length);
                  }

                  decoder.releaseOutputBuffer(outIndex, false);
               }
            }
         } catch (Exception var18) {
            var18.printStackTrace();
         }

         if ((bufferInfo.flags & 4) != 0) {
            Log.d(this.TAG, "OutputBuffer BUFFER_FLAG_END_OF_STREAM");
            break;
         }
      }

      audioTrack.flush();
      audioTrack.release();
      decoder.stop();
      decoder.release();
      this.audioFrameQueue.clear();
      if (this.DEBUG) {
         Log.d(this.TAG, "AudioDecodeThread stopped");
      }

   }

   public AudioDecodeThread(@NotNull String mimeType, int sampleRate, int channelCount, @Nullable byte[] codecConfig, @NotNull FrameQueue audioFrameQueue) {
      //Intrinsics.checkNotNullParameter(mimeType, "mimeType");
      //Intrinsics.checkNotNullParameter(audioFrameQueue, "audioFrameQueue");
      super();


	  //String var10001 = AudioDecodeThread.class.getSimpleName();
      //Intrinsics.checkNotNullExpressionValue(var10001, "AudioDecodeThread::class.java.simpleName");
      //this.TAG = var10001;
	  this.TAG = AudioDecodeThread.class.getSimpleName();
      this.DEBUG = true;

	  if(mimeType == null || audioFrameQueue == null){
		  Log.e(this.TAG, "KOTLIN: null mimeType, audioFrameQueue");
	  }


      this.mimeType = mimeType;
      this.sampleRate = sampleRate;
      this.channelCount = channelCount;
      this.codecConfig = codecConfig;
      this.audioFrameQueue = audioFrameQueue;
      
   }

   /*
   @Metadata(
      mv = {1, 4, 2},
      bv = {1, 0, 3},
      k = 1,
      d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006¨\u0006\t"},
      d2 = {"Lcom/tgm/cordova/rtspplayer/decode/AudioDecodeThread$Companion;", "", "()V", "getAacDecoderConfigData", "", "audioProfile", "", "sampleRate", "channels", "rtsp-client-android.app"}
   )
   */

   public static final class Companion {
      @NotNull
      public final byte[] getAacDecoderConfigData(int audioProfile, int sampleRate, int channels) {
         int extraDataAac = audioProfile << 11;
         switch(sampleRate) {
         case 7350:
            extraDataAac |= 1536;
            break;
         case 8000:
            extraDataAac |= 1408;
            break;
         case 11025:
            extraDataAac |= 1280;
            break;
         case 12000:
            extraDataAac |= 1152;
            break;
         case 16000:
            extraDataAac |= 1024;
            break;
         case 22050:
            extraDataAac |= 896;
            break;
         case 24000:
            extraDataAac |= 768;
            break;
         case 32000:
            extraDataAac |= 640;
            break;
         case 44100:
            extraDataAac |= 512;
            break;
         case 48000:
            extraDataAac |= 384;
            break;
         case 64000:
            extraDataAac |= 256;
            break;
         case 88200:
            extraDataAac |= 128;
            break;
         case 96000:
            extraDataAac |= 0;
         }

         extraDataAac |= channels << 3;
         byte[] extraData = new byte[]{(byte)((extraDataAac & '\uff00') >> 8), (byte)(extraDataAac & 255)};
         return extraData;
      }

      private Companion() {
      }

	  /*
      // $FF: synthetic method
      public Companion(DefaultConstructorMarker $constructor_marker) {
         this();
      }
	  */
   }
}

