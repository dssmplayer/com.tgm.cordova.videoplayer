// LiveFragmentKt.java
package com.tgm.cordova.rtspplayer.ui.live;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(
   mv = {1, 4, 2},
   bv = {1, 0, 3},
   k = 2,
   d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"},
   d2 = {"DEBUG", "", "DEFAULT_RTSP_PORT", "", "TAG", "", "rtsp-client-android.app"}
)
public final class LiveFragmentKt {
   private static final int DEFAULT_RTSP_PORT = 554;
   private static final String TAG;
   private static final boolean DEBUG = true;

   static {
      String var10000 = LiveFragment.class.getSimpleName();
      Intrinsics.checkNotNullExpressionValue(var10000, "LiveFragment::class.java.simpleName");
      TAG = var10000;
   }

   // $FF: synthetic method
   public static final String access$getTAG$p() {
      return TAG;
   }
}



