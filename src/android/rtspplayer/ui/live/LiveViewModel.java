// LiveViewModel.java
package com.tgm.cordova.rtspplayer.ui.live;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import kotlin.Metadata;
import kotlin.Unit;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(
   mv = {1, 4, 2},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u0010\u0010\u0010\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0007R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u0007¨\u0006\u0011"},
   d2 = {"Lcom/tgm/cordova/rtspplayer/ui/live/LiveViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "rtspPassword", "Landroidx/lifecycle/MutableLiveData;", "", "getRtspPassword", "()Landroidx/lifecycle/MutableLiveData;", "rtspRequest", "getRtspRequest", "rtspUsername", "getRtspUsername", "loadParams", "", "context", "Landroid/content/Context;", "saveParams", "rtsp-client-android.app"}
)
public final class LiveViewModel extends ViewModel {
   @NotNull
   private final MutableLiveData rtspRequest;
   @NotNull
   private final MutableLiveData rtspUsername;
   @NotNull
   private final MutableLiveData rtspPassword;

   @NotNull
   public final MutableLiveData getRtspRequest() {
      return this.rtspRequest;
   }

   @NotNull
   public final MutableLiveData getRtspUsername() {
      return this.rtspUsername;
   }

   @NotNull
   public final MutableLiveData getRtspPassword() {
      return this.rtspPassword;
   }

   public final void loadParams(@Nullable Context context) {
      Log.v(LiveViewModelKt.access$getTAG$p(), "loadParams()");
      SharedPreferences pref = context != null ? context.getSharedPreferences("live_params", 0) : null;

      try {
         this.rtspRequest.setValue(pref != null ? pref.getString("rtsp_request", "rtsp://10.6.21.163:554/profile2/media.smp") : null);
      } catch (ClassCastException var6) {
         var6.printStackTrace();
      }

      try {
         this.rtspUsername.setValue(pref != null ? pref.getString("rtsp_username", "") : null);
      } catch (ClassCastException var5) {
         var5.printStackTrace();
      }

      try {
         this.rtspPassword.setValue(pref != null ? pref.getString("rtsp_password", "") : null);
      } catch (ClassCastException var4) {
         var4.printStackTrace();
      }

   }

   public final void saveParams(@Nullable Context context) {
      Editor var3;
      label32: {
         Log.v(LiveViewModelKt.access$getTAG$p(), "saveParams()");
         if (context != null) {
            SharedPreferences var10000 = context.getSharedPreferences("live_params", 0);
            if (var10000 != null) {
               var3 = var10000.edit();
               break label32;
            }
         }

         var3 = null;
      }

      Editor editor = var3;
      if (editor != null) {
         editor.putString("rtsp_request", (String)this.rtspRequest.getValue());
      }

      if (editor != null) {
         editor.putString("rtsp_username", (String)this.rtspUsername.getValue());
      }

      if (editor != null) {
         editor.putString("rtsp_password", (String)this.rtspPassword.getValue());
      }

      if (editor != null) {
         editor.apply();
      }

   }

   public LiveViewModel() {
      MutableLiveData var1 = new MutableLiveData();
      boolean var2 = false;
      boolean var3 = false;
      int var5 = false;
      var1.setValue("rtsp://10.6.21.163:554/profile2/media.smp");
      Unit var7 = Unit.INSTANCE;
      this.rtspRequest = var1;
      var1 = new MutableLiveData();
      var2 = false;
      var3 = false;
      var5 = false;
      var1.setValue("");
      var7 = Unit.INSTANCE;
      this.rtspUsername = var1;
      var1 = new MutableLiveData();
      var2 = false;
      var3 = false;
      var5 = false;
      var1.setValue("");
      var7 = Unit.INSTANCE;
      this.rtspPassword = var1;
   }
}


