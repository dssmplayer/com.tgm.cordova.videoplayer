// LiveFragment.java
package com.tgm.cordova.rtspplayer.ui.live;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.SurfaceHolder.Callback;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import com.alexvas.rtsp.RtspClient;
import com.alexvas.rtsp.RtspClient.AudioTrack;
import com.alexvas.rtsp.RtspClient.Builder;
import com.alexvas.rtsp.RtspClient.RtspClientListener;
import com.alexvas.rtsp.RtspClient.SdpInfo;
import com.alexvas.rtsp.RtspClient.VideoTrack;
import com.tgm.cordova.rtspplayer.decode.AudioDecodeThread;
import com.tgm.cordova.rtspplayer.decode.FrameQueue;
import com.tgm.cordova.rtspplayer.decode.VideoDecodeThread;
import com.tgm.cordova.rtspplayer.decode.FrameQueue.Frame;
import com.alexvas.utils.NetUtils;
import com.alexvas.utils.VideoCodecUtils;
import com.alexvas.utils.VideoCodecUtils.NalUnit;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.collections.ArraysKt;
import kotlin.jvm.internal.Intrinsics;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Metadata(
   mv = {1, 4, 2},
   bv = {1, 0, 3},
   k = 1,
   d1 = {"\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001@B\u0005¢\u0006\u0002\u0010\u0003J&\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020-2\b\u0010.\u001a\u0004\u0018\u00010/2\b\u00100\u001a\u0004\u0018\u000101H\u0016J\u0006\u00102\u001a\u000203J\u0006\u00104\u001a\u000203J\u0006\u00105\u001a\u000203J\b\u00106\u001a\u000203H\u0016J\b\u00107\u001a\u000203H\u0016J(\u00108\u001a\u0002032\u0006\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020\u00052\u0006\u0010<\u001a\u00020\u00052\u0006\u0010=\u001a\u00020\u0005H\u0016J\u0010\u0010>\u001a\u0002032\u0006\u00109\u001a\u00020:H\u0016J\u0010\u0010?\u001a\u0002032\u0006\u00109\u001a\u00020:H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0012X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0018\u00010\u0019R\u00020\u0000X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0005X\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0082\u000e¢\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010!X\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u000bX\u0082\u000e¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\rX\u0082\u000e¢\u0006\u0002\n\u0000R\u001a\u0010$\u001a\u00020%X\u0086\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)¨\u0006A"},
   d2 = {"Lcom/tgm/cordova/rtspplayer/ui/live/LiveFragment;", "Landroidx/fragment/app/Fragment;", "Landroid/view/SurfaceHolder$Callback;", "()V", "audioChannelCount", "", "audioCodecConfig", "", "audioDecodeThread", "Lcom/tgm/cordova/rtspplayer/decode/AudioDecodeThread;", "audioFrameQueue", "Lcom/tgm/cordova/rtspplayer/decode/FrameQueue;", "audioMimeType", "", "audioSampleRate", "btnStartStop", "Landroid/widget/Button;", "checkAudio", "Landroid/widget/CheckBox;", "checkVideo", "liveViewModel", "Lcom/tgm/cordova/rtspplayer/ui/live/LiveViewModel;", "rtspStopped", "Ljava/util/concurrent/atomic/AtomicBoolean;", "rtspThread", "Lcom/tgm/cordova/rtspplayer/ui/live/LiveFragment$RtspThread;", "surface", "Landroid/view/Surface;", "surfaceHeight", "surfaceWidth", "textStatus", "Landroid/widget/TextView;", "videoDecodeThread", "Lcom/tgm/cordova/rtspplayer/decode/VideoDecodeThread;", "videoFrameQueue", "videoMimeType", "vqPushCount", "", "getVqPushCount", "()J", "setVqPushCount", "(J)V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onRtspClientConnected", "", "onRtspClientStarted", "onRtspClientStopped", "onStart", "onStop", "surfaceChanged", "holder", "Landroid/view/SurfaceHolder;", "format", "width", "height", "surfaceCreated", "surfaceDestroyed", "RtspThread", "rtsp-client-android.app"}
)
public final class LiveFragment extends Fragment implements Callback {
   private long vqPushCount;
   private LiveViewModel liveViewModel;
   private FrameQueue videoFrameQueue = new FrameQueue();
   private FrameQueue audioFrameQueue = new FrameQueue();
   private LiveFragment.RtspThread rtspThread;
   private VideoDecodeThread videoDecodeThread;
   private AudioDecodeThread audioDecodeThread;
   private AtomicBoolean rtspStopped = new AtomicBoolean(true);
   private Button btnStartStop;
   private Surface surface;
   private int surfaceWidth = 1920;
   private int surfaceHeight = 1080;
   private CheckBox checkVideo;
   private CheckBox checkAudio;
   private TextView textStatus;
   private String videoMimeType = "";
   private String audioMimeType = "";
   private int audioSampleRate;
   private int audioChannelCount;
   private byte[] audioCodecConfig;
   private HashMap _$_findViewCache;

   public final long getVqPushCount() {
      return this.vqPushCount;
   }

   public final void setVqPushCount(long var1) {
      this.vqPushCount = var1;
   }

   public final void onRtspClientStarted() {
      Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspClientStarted()");
      this.rtspStopped.set(false);
      Button var10000 = this.btnStartStop;
      if (var10000 != null) {
         var10000.setText((CharSequence)"Stop RTSP");
      }

   }

   public final void onRtspClientStopped() {
      Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspClientStopped()");
      this.rtspStopped.set(true);
      Button var10000 = this.btnStartStop;
      if (var10000 != null) {
         var10000.setText((CharSequence)"Start RTSP");
      }

      VideoDecodeThread var1 = this.videoDecodeThread;
      if (var1 != null) {
         var1.interrupt();
      }

      this.videoDecodeThread = (VideoDecodeThread)null;
      AudioDecodeThread var2 = this.audioDecodeThread;
      if (var2 != null) {
         var2.interrupt();
      }

      this.audioDecodeThread = (AudioDecodeThread)null;
      this.vqPushCount = 0L;
   }

   public final void onRtspClientConnected() {
      Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspClientConnected()");
      CharSequence var1 = (CharSequence)this.videoMimeType;
      boolean var2 = false;
      CheckBox var10000;
      if (var1.length() > 0) {
         var10000 = this.checkVideo;
         Intrinsics.checkNotNull(var10000);
         if (var10000.isChecked()) {
            Log.i(LiveFragmentKt.access$getTAG$p(), "Starting video decoder with mime type \"" + this.videoMimeType + '"');
            Surface var10003 = this.surface;
            Intrinsics.checkNotNull(var10003);
            this.videoDecodeThread = new VideoDecodeThread(var10003, this.videoMimeType, this.surfaceWidth, this.surfaceHeight, this.videoFrameQueue);
            VideoDecodeThread var3 = this.videoDecodeThread;
            if (var3 != null) {
               var3.start();
            }
         }
      }

      var1 = (CharSequence)this.audioMimeType;
      var2 = false;
      if (var1.length() > 0) {
         var10000 = this.checkAudio;
         Intrinsics.checkNotNull(var10000);
         if (var10000.isChecked()) {
            Log.i(LiveFragmentKt.access$getTAG$p(), "Starting audio decoder with mime type \"" + this.audioMimeType + '"');
            this.audioDecodeThread = new AudioDecodeThread(this.audioMimeType, this.audioSampleRate, this.audioChannelCount, this.audioCodecConfig, this.audioFrameQueue);
            AudioDecodeThread var4 = this.audioDecodeThread;
            if (var4 != null) {
               var4.start();
            }
         }
      }

   }

   @Nullable
   public View onCreateView(@NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
      Intrinsics.checkNotNullParameter(inflater, "inflater");
      ViewModel var10001 = (new ViewModelProvider((ViewModelStoreOwner)this)).get(LiveViewModel.class);
      Intrinsics.checkNotNullExpressionValue(var10001, "ViewModelProvider(this).…iveViewModel::class.java)");
      this.liveViewModel = (LiveViewModel)var10001;
      View root = inflater.inflate(1300078, container, false);
      View var10000 = root.findViewById(1000140);
      Intrinsics.checkNotNullExpressionValue(var10000, "root.findViewById(R.id.edit_rtsp_request)");
      final EditText textRtspRequest = (EditText)var10000;
      var10000 = root.findViewById(1000313);
      Intrinsics.checkNotNullExpressionValue(var10000, "root.findViewById(R.id.edit_rtsp_username)");
      final EditText textRtspUsername = (EditText)var10000;
      var10000 = root.findViewById(1000039);
      Intrinsics.checkNotNullExpressionValue(var10000, "root.findViewById(R.id.edit_rtsp_password)");
      final EditText textRtspPassword = (EditText)var10000;
      var10000 = root.findViewById(1000073);
      Intrinsics.checkNotNullExpressionValue(var10000, "root.findViewById(R.id.surfaceView)");
      SurfaceView surfaceView = (SurfaceView)var10000;
      this.checkVideo = (CheckBox)root.findViewById(1000119);
      this.checkAudio = (CheckBox)root.findViewById(1000117);
      this.textStatus = (TextView)root.findViewById(1000191);
      surfaceView.getHolder().addCallback((Callback)this);
      SurfaceHolder var9 = surfaceView.getHolder();
      Intrinsics.checkNotNullExpressionValue(var9, "surfaceView.holder");
      this.surface = var9.getSurface();
      textRtspRequest.addTextChangedListener((TextWatcher)(new TextWatcher() {
         public void afterTextChanged(@Nullable Editable s) {
         }

         public void beforeTextChanged(@Nullable CharSequence s, int start, int count, int after) {
         }

         public void onTextChanged(@Nullable CharSequence s, int start, int before, int count) {
            String text = String.valueOf(s);
            if (Intrinsics.areEqual(text, (String)LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspRequest().getValue()) ^ true) {
               LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspRequest().setValue(text);
            }

         }
      }));
      textRtspUsername.addTextChangedListener((TextWatcher)(new TextWatcher() {
         public void afterTextChanged(@Nullable Editable s) {
         }

         public void beforeTextChanged(@Nullable CharSequence s, int start, int count, int after) {
         }

         public void onTextChanged(@Nullable CharSequence s, int start, int before, int count) {
            String text = String.valueOf(s);
            if (Intrinsics.areEqual(text, (String)LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspUsername().getValue()) ^ true) {
               LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspUsername().setValue(text);
            }

         }
      }));
      textRtspPassword.addTextChangedListener((TextWatcher)(new TextWatcher() {
         public void afterTextChanged(@Nullable Editable s) {
         }

         public void beforeTextChanged(@Nullable CharSequence s, int start, int count, int after) {
         }

         public void onTextChanged(@Nullable CharSequence s, int start, int before, int count) {
            String text = String.valueOf(s);
            if (Intrinsics.areEqual(text, (String)LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspPassword().getValue()) ^ true) {
               LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspPassword().setValue(text);
            }

         }
      }));
      LiveViewModel var10 = this.liveViewModel;
      if (var10 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("liveViewModel");
      }

      var10.getRtspRequest().observe(this.getViewLifecycleOwner(), (Observer)(new Observer() {
         // $FF: synthetic method
         // $FF: bridge method
         public void onChanged(Object var1) {
            this.onChanged((String)var1);
         }

         public final void onChanged(String it) {
            if (Intrinsics.areEqual(textRtspRequest.getText().toString(), it) ^ true) {
               textRtspRequest.setText((CharSequence)it);
            }

         }
      }));
      var10 = this.liveViewModel;
      if (var10 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("liveViewModel");
      }

      var10.getRtspUsername().observe(this.getViewLifecycleOwner(), (Observer)(new Observer() {
         // $FF: synthetic method
         // $FF: bridge method
         public void onChanged(Object var1) {
            this.onChanged((String)var1);
         }

         public final void onChanged(String it) {
            if (Intrinsics.areEqual(textRtspUsername.getText().toString(), it) ^ true) {
               textRtspUsername.setText((CharSequence)it);
            }

         }
      }));
      var10 = this.liveViewModel;
      if (var10 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("liveViewModel");
      }

      var10.getRtspPassword().observe(this.getViewLifecycleOwner(), (Observer)(new Observer() {
         // $FF: synthetic method
         // $FF: bridge method
         public void onChanged(Object var1) {
            this.onChanged((String)var1);
         }

         public final void onChanged(String it) {
            if (Intrinsics.areEqual(textRtspPassword.getText().toString(), it) ^ true) {
               textRtspPassword.setText((CharSequence)it);
            }

         }
      }));
      this.btnStartStop = (Button)root.findViewById(1000027);
      Button var11 = this.btnStartStop;
      if (var11 != null) {
         var11.setOnClickListener((OnClickListener)(new OnClickListener() {
            public final void onClick(View it) {
               LiveFragment.RtspThread var10000;
               if (LiveFragment.this.rtspStopped.get()) {
                  Log.d(LiveFragmentKt.access$getTAG$p(), "Starting RTSP thread...");
                  LiveFragment.this.rtspThread = LiveFragment.this.new RtspThread();
                  var10000 = LiveFragment.this.rtspThread;
                  if (var10000 != null) {
                     var10000.start();
                  }
               } else {
                  Log.d(LiveFragmentKt.access$getTAG$p(), "Stopping RTSP thread...");
                  LiveFragment.this.rtspStopped.set(true);
                  var10000 = LiveFragment.this.rtspThread;
                  if (var10000 != null) {
                     var10000.interrupt();
                  }
               }

            }
         }));
      }

      return root;
   }

   public void onStart() {
      Log.v(LiveFragmentKt.access$getTAG$p(), "onStart()");
      super.onStart();
      LiveViewModel var10000 = this.liveViewModel;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("liveViewModel");
      }

      var10000.loadParams(this.getContext());
   }

   public void onStop() {
      Log.v(LiveFragmentKt.access$getTAG$p(), "onStop()");
      super.onStop();
      LiveViewModel var10000 = this.liveViewModel;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("liveViewModel");
      }

      var10000.saveParams(this.getContext());
      this.onRtspClientStopped();
   }

   public void surfaceChanged(@NotNull SurfaceHolder holder, int format, int width, int height) {
      Intrinsics.checkNotNullParameter(holder, "holder");
      Log.v(LiveFragmentKt.access$getTAG$p(), "surfaceChanged(format=" + format + ", width=" + width + ", height=" + height + ')');
      this.surface = holder.getSurface();
      this.surfaceWidth = width;
      this.surfaceHeight = height;
      if (this.videoDecodeThread != null) {
         VideoDecodeThread var10000 = this.videoDecodeThread;
         if (var10000 != null) {
            var10000.interrupt();
         }

         Surface var10003 = this.surface;
         Intrinsics.checkNotNull(var10003);
         this.videoDecodeThread = new VideoDecodeThread(var10003, this.videoMimeType, width, height, this.videoFrameQueue);
         var10000 = this.videoDecodeThread;
         if (var10000 != null) {
            var10000.start();
         }
      }

   }

   public void surfaceDestroyed(@NotNull SurfaceHolder holder) {
      Intrinsics.checkNotNullParameter(holder, "holder");
      Log.v(LiveFragmentKt.access$getTAG$p(), "surfaceDestroyed()");
      VideoDecodeThread var10000 = this.videoDecodeThread;
      if (var10000 != null) {
         var10000.interrupt();
      }

      this.videoDecodeThread = (VideoDecodeThread)null;
   }

   public void surfaceCreated(@NotNull SurfaceHolder holder) {
      Intrinsics.checkNotNullParameter(holder, "holder");
      Log.v(LiveFragmentKt.access$getTAG$p(), "surfaceCreated()");
   }

   // $FF: synthetic method
   public static final void access$setTextStatus$p(LiveFragment $this, TextView var1) {
      $this.textStatus = var1;
   }

   // $FF: synthetic method
   public static final void access$setRtspStopped$p(LiveFragment $this, AtomicBoolean var1) {
      $this.rtspStopped = var1;
   }

   // $FF: synthetic method
   public static final void access$setVideoFrameQueue$p(LiveFragment $this, FrameQueue var1) {
      $this.videoFrameQueue = var1;
   }

   // $FF: synthetic method
   public static final String access$getVideoMimeType$p(LiveFragment $this) {
      return $this.videoMimeType;
   }

   // $FF: synthetic method
   public static final String access$getAudioMimeType$p(LiveFragment $this) {
      return $this.audioMimeType;
   }

   // $FF: synthetic method
   public static final void access$setVideoDecodeThread$p(LiveFragment $this, VideoDecodeThread var1) {
      $this.videoDecodeThread = var1;
   }

   // $FF: synthetic method
   public static final void access$setAudioFrameQueue$p(LiveFragment $this, FrameQueue var1) {
      $this.audioFrameQueue = var1;
   }

   // $FF: synthetic method
   public static final int access$getAudioSampleRate$p(LiveFragment $this) {
      return $this.audioSampleRate;
   }

   // $FF: synthetic method
   public static final int access$getAudioChannelCount$p(LiveFragment $this) {
      return $this.audioChannelCount;
   }

   // $FF: synthetic method
   public static final byte[] access$getAudioCodecConfig$p(LiveFragment $this) {
      return $this.audioCodecConfig;
   }

   // $FF: synthetic method
   public static final LiveViewModel access$getLiveViewModel$p(LiveFragment $this) {
      LiveViewModel var10000 = $this.liveViewModel;
      if (var10000 == null) {
         Intrinsics.throwUninitializedPropertyAccessException("liveViewModel");
      }

      return var10000;
   }

   // $FF: synthetic method
   public static final void access$setLiveViewModel$p(LiveFragment $this, LiveViewModel var1) {
      $this.liveViewModel = var1;
   }

   // $FF: synthetic method
   public static final void access$setCheckVideo$p(LiveFragment $this, CheckBox var1) {
      $this.checkVideo = var1;
   }

   // $FF: synthetic method
   public static final void access$setCheckAudio$p(LiveFragment $this, CheckBox var1) {
      $this.checkAudio = var1;
   }

   public View _$_findCachedViewById(int var1) {
      if (this._$_findViewCache == null) {
         this._$_findViewCache = new HashMap();
      }

      View var2 = (View)this._$_findViewCache.get(var1);
      if (var2 == null) {
         View var10000 = this.getView();
         if (var10000 == null) {
            return null;
         }

         var2 = var10000.findViewById(var1);
         this._$_findViewCache.put(var1, var2);
      }

      return var2;
   }

   public void _$_clearFindViewByIdCache() {
      if (this._$_findViewCache != null) {
         this._$_findViewCache.clear();
      }

   }

   // $FF: synthetic method
   public void onDestroyView() {
      super.onDestroyView();
      this._$_clearFindViewByIdCache();
   }

   @Metadata(
      mv = {1, 4, 2},
      bv = {1, 0, 3},
      k = 1,
      d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"},
      d2 = {"Lcom/tgm/cordova/rtspplayer/ui/live/LiveFragment$RtspThread;", "Ljava/lang/Thread;", "(Lcom/tgm/cordova/rtspplayer/ui/live/LiveFragment;)V", "run", "", "rtsp-client-android.app"}
   )
   public final class RtspThread extends Thread {
      public void run() {
         (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
            public final void run() {
               LiveFragment.this.onRtspClientStarted();
            }
         }));
         RtspClientListener listener = new RtspClientListener() {
            public void onRtspDisconnected() {
               Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspDisconnected()");
               (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
                  public final void run() {
                     TextView var10000 = LiveFragment.this.textStatus;
                     if (var10000 != null) {
                        var10000.setText((CharSequence)"RTSP disconnected");
                     }

                  }
               }));
               LiveFragment.this.rtspStopped.set(true);
            }

            public void onRtspFailed(@Nullable final String message) {
               Log.e(LiveFragmentKt.access$getTAG$p(), "onRtspFailed(message=\"" + message + "\")");
               (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
                  public final void run() {
                     TextView var10000 = LiveFragment.this.textStatus;
                     if (var10000 != null) {
                        var10000.setText((CharSequence)("RTSP failed: " + message));
                     }

                  }
               }));
               LiveFragment.this.rtspStopped.set(true);
            }

            public void onRtspConnected(@NotNull final SdpInfo sdpInfo) {
               Intrinsics.checkNotNullParameter(sdpInfo, "sdpInfo");
               Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspConnected()");
               (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
                  public final void run() {
                     String s = "";
                     if (sdpInfo.videoTrack != null) {
                        s = "video";
                     }

                     if (sdpInfo.audioTrack != null) {
                        if (s.length() > 0) {
                           s = s + ", ";
                        }

                        s = s + "audio";
                     }

                     TextView var10000 = LiveFragment.this.textStatus;
                     if (var10000 != null) {
                        var10000.setText((CharSequence)("RTSP connected (" + s + ')'));
                     }

                  }
               }));
               Integer var2;
               boolean var3;
               AudioTrack var10;
               LiveFragment var11;
               if (sdpInfo.videoTrack != null) {
                  VideoTrack var10000;
                  label94: {
                     LiveFragment.this.videoFrameQueue.clear();
                     var10000 = sdpInfo.videoTrack;
                     var2 = var10000 != null ? var10000.videoCodec : null;
                     var3 = false;
                     if (var2 != null) {
                        if (var2 == 0) {
                           LiveFragment.this.videoMimeType = "video/avc";
                           break label94;
                        }
                     }

                     byte var8 = 1;
                     if (var2 != null) {
                        if (var2 == var8) {
                           LiveFragment.this.videoMimeType = "video/hevc";
                        }
                     }
                  }

                  var10 = sdpInfo.audioTrack;
                  var2 = var10 != null ? var10.audioCodec : null;
                  var3 = false;
                  if (var2 != null) {
                     if (var2 == 0) {
                        LiveFragment.this.audioMimeType = "audio/mp4a-latm";
                     }
                  }

                  var10000 = sdpInfo.videoTrack;
                  byte[] sps = var10000 != null ? var10000.sps : null;
                  var10000 = sdpInfo.videoTrack;
                  byte[] pps = var10000 != null ? var10000.pps : null;
                  if (sps != null && pps != null) {
                     byte[] data = new byte[sps.length + pps.length];
                     ArraysKt.copyInto(sps, data, 0, 0, sps.length);
                     ArraysKt.copyInto(pps, data, sps.length, 0, pps.length);
                     LiveFragment.this.videoFrameQueue.push(new Frame(data, 0, data.length, 0L));
                     var11 = LiveFragment.this;
                     var11.setVqPushCount(var11.getVqPushCount() + 1L);
                     String var13 = LiveFragmentKt.access$getTAG$p();
                     StringBuilder var10001 = (new StringBuilder()).append("count_info: ").append(LiveFragment.this.getVqPushCount()).append(',');
                     VideoDecodeThread var10002 = LiveFragment.this.videoDecodeThread;
                     var10001 = var10001.append(var10002 != null ? var10002.getVqPopCount() : null).append(',');
                     var10002 = LiveFragment.this.videoDecodeThread;
                     Log.d(var13, var10001.append(var10002 != null ? var10002.getRenderCount() : null).toString());
                  } else {
                     Log.d(LiveFragmentKt.access$getTAG$p(), "RTSP SPS and PPS NAL units missed in SDP");
                  }
               }

               if (sdpInfo.audioTrack != null) {
                  LiveFragment.this.audioFrameQueue.clear();
                  var10 = sdpInfo.audioTrack;
                  var2 = var10 != null ? var10.audioCodec : null;
                  var3 = false;
                  if (var2 != null) {
                     if (var2 == 0) {
                        LiveFragment.this.audioMimeType = "audio/mp4a-latm";
                     }
                  }

                  var11 = LiveFragment.this;
                  AudioTrack var12 = sdpInfo.audioTrack;
                  Integer var14 = var12 != null ? var12.sampleRateHz : null;
                  Intrinsics.checkNotNull(var14);
                  var11.audioSampleRate = var14;
                  var11 = LiveFragment.this;
                  var12 = sdpInfo.audioTrack;
                  var14 = var12 != null ? var12.channels : null;
                  Intrinsics.checkNotNull(var14);
                  var11.audioChannelCount = var14;
                  var12 = sdpInfo.audioTrack;
                  LiveFragment.this.audioCodecConfig = var12 != null ? var12.config : null;
               }

               LiveFragment.this.onRtspClientConnected();
            }

            public void onRtspFailedUnauthorized() {
               Log.e(LiveFragmentKt.access$getTAG$p(), "onRtspFailedUnauthorized()");
               (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
                  public final void run() {
                     TextView var10000 = LiveFragment.this.textStatus;
                     if (var10000 != null) {
                        var10000.setText((CharSequence)"RTSP username or password is incorrect");
                     }

                  }
               }));
               LiveFragment.this.rtspStopped.set(true);
            }

            public void onRtspVideoNalUnitReceived(@NotNull byte[] data, int offset, int length, long timestamp) {
               Intrinsics.checkNotNullParameter(data, "data");
               Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspVideoNalUnitReceived(length=" + length + ", timestamp=" + timestamp + ')');
               ArrayList nals = new ArrayList();
               int numNals = VideoCodecUtils.getH264NalUnits(data, offset, length - 1, nals);
               StringBuilder builder = new StringBuilder();
               Iterator var10 = nals.iterator();

               while(var10.hasNext()) {
                  NalUnit nal = (NalUnit)var10.next();
                  builder.append(", ");
                  builder.append(VideoCodecUtils.getH264NalUnitTypeString(nal.type));
                  builder.append(" (");
                  builder.append(nal.length);
                  builder.append(" bytes)");
               }

               String var10000 = builder.toString();
               Intrinsics.checkNotNullExpressionValue(var10000, "builder.toString()");
               String textNals = var10000;
               if (numNals > 0) {
                  byte var11 = 2;
                  boolean var12 = false;
                  if (textNals == null) {
                     throw new NullPointerException("null cannot be cast to non-null type java.lang.String");
                  }

                  var10000 = textNals.substring(var11);
                  Intrinsics.checkNotNullExpressionValue(var10000, "(this as java.lang.String).substring(startIndex)");
                  textNals = var10000;
               }

               Log.i(LiveFragmentKt.access$getTAG$p(), "NALs (" + numNals + "): " + textNals);
               if (length > 0) {
                  LiveFragment.this.videoFrameQueue.push(new Frame(data, offset, length, timestamp));
                  LiveFragment var14 = LiveFragment.this;
                  var14.setVqPushCount(var14.getVqPushCount() + 1L);
                  var10000 = LiveFragmentKt.access$getTAG$p();
                  StringBuilder var10001 = (new StringBuilder()).append("count_info__: ").append(LiveFragment.this.getVqPushCount()).append(',');
                  VideoDecodeThread var10002 = LiveFragment.this.videoDecodeThread;
                  var10001 = var10001.append(var10002 != null ? var10002.getVqPopCount() : null).append(',');
                  var10002 = LiveFragment.this.videoDecodeThread;
                  Log.d(var10000, var10001.append(var10002 != null ? var10002.getRenderCount() : null).toString());
               }

            }

            public void onRtspAudioSampleReceived(@NotNull byte[] data, int offset, int length, long timestamp) {
               Intrinsics.checkNotNullParameter(data, "data");
               Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspAudioSampleReceived(length=" + length + ", timestamp=" + timestamp + ')');
               if (length > 0) {
                  LiveFragment.this.audioFrameQueue.push(new Frame(data, offset, length, timestamp));
               }

            }

            public void onRtspConnecting() {
               Log.v(LiveFragmentKt.access$getTAG$p(), "onRtspConnecting()");
               (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
                  public final void run() {
                     TextView var10000 = LiveFragment.this.textStatus;
                     if (var10000 != null) {
                        var10000.setText((CharSequence)"RTSP connecting");
                     }

                  }
               }));
            }
         };
         Uri var10000 = Uri.parse((String)LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspRequest().getValue());
         Intrinsics.checkNotNullExpressionValue(var10000, "Uri.parse(liveViewModel.rtspRequest.value)");
         Uri uri = var10000;
         int port = uri.getPort() == -1 ? 554 : uri.getPort();
         String username = (String)LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspUsername().getValue();
         String password = (String)LiveFragment.access$getLiveViewModel$p(LiveFragment.this).getRtspPassword().getValue();

         try {
            Log.d(LiveFragmentKt.access$getTAG$p(), "Connecting to " + uri.getHost() + ':' + port + "...");
            Socket var9 = NetUtils.createSocketAndConnect(String.valueOf(uri.getHost()), port, 5000);
            Intrinsics.checkNotNullExpressionValue(var9, "NetUtils.createSocketAnd…t.toString(), port, 5000)");
            Socket socket = var9;
            Builder var10 = new Builder(socket, uri.toString(), LiveFragment.this.rtspStopped, (RtspClientListener)listener);
            CheckBox var10001 = LiveFragment.this.checkVideo;
            Intrinsics.checkNotNull(var10001);
            var10 = var10.requestVideo(var10001.isChecked());
            var10001 = LiveFragment.this.checkAudio;
            Intrinsics.checkNotNull(var10001);
            RtspClient var11 = var10.requestAudio(var10001.isChecked()).withDebug(true).withUserAgent("RTSP test").withCredentials(username, password).build();
            Intrinsics.checkNotNullExpressionValue(var11, "RtspClient.Builder(socke…                 .build()");
            RtspClient rtspClient = var11;
            rtspClient.execute();
            NetUtils.closeSocket(socket);
         } catch (Exception var8) {
            var8.printStackTrace();
         }

         (new Handler(Looper.getMainLooper())).post((Runnable)(new Runnable() {
            public final void run() {
               LiveFragment.this.onRtspClientStopped();
            }
         }));
      }
   }
}


