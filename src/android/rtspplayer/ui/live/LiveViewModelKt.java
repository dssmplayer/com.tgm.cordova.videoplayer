// LiveViewModelKt.java
package com.tgm.cordova.rtspplayer.ui.live;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

@Metadata(
   mv = {1, 4, 2},
   bv = {1, 0, 3},
   k = 2,
   d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\b\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0003X\u0082T¢\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0003X\u0082\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"},
   d2 = {"DEBUG", "", "DEFAULT_RTSP_PASSWORD", "", "DEFAULT_RTSP_REQUEST", "DEFAULT_RTSP_USERNAME", "LIVE_PARAMS_FILENAME", "RTSP_PASSWORD_KEY", "RTSP_REQUEST_KEY", "RTSP_USERNAME_KEY", "TAG", "rtsp-client-android.app"}
)
public final class LiveViewModelKt {
   private static final String RTSP_REQUEST_KEY = "rtsp_request";
   private static final String RTSP_USERNAME_KEY = "rtsp_username";
   private static final String RTSP_PASSWORD_KEY = "rtsp_password";
   private static final String DEFAULT_RTSP_REQUEST = "rtsp://10.6.21.163:554/profile2/media.smp";
   private static final String DEFAULT_RTSP_USERNAME = "";
   private static final String DEFAULT_RTSP_PASSWORD = "";
   private static final String TAG;
   private static final boolean DEBUG = true;
   private static final String LIVE_PARAMS_FILENAME = "live_params";

   static {
      String var10000 = LiveViewModel.class.getSimpleName();
      Intrinsics.checkNotNullExpressionValue(var10000, "LiveViewModel::class.java.simpleName");
      TAG = var10000;
   }

   // $FF: synthetic method
   public static final String access$getTAG$p() {
      return TAG;
   }
}

