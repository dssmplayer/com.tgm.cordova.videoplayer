package com.tgm.cordova.videoplayer;

import android.widget.LinearLayout;
import android.app.Fragment;
import android.content.Context;
import android.media.AudioManager;
import android.util.Base64;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.util.DisplayMetrics;
//import android.util.Size;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import android.view.TextureView;
import android.graphics.SurfaceTexture;
import android.graphics.Color;
import android.graphics.PorterDuff;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.apache.cordova.LOG;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.Exception;
import java.lang.Integer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import java.util.UUID;
import android.view.PixelCopy;
import android.os.Handler;
import android.os.HandlerThread;
import android.widget.Toast;
import android.view.Gravity;
import android.view.WindowManager.LayoutParams;
import com.tgm.cordova.videoplayer.rtspplayer.RtspPlayer;
import com.tgm.cordova.videoplayer.rtspplayer.RtspPlayer.RtspPlayerListener;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
//import android.widget.VideoView;
import android.net.Uri;
import android.media.AudioTrack;
import java.lang.reflect.Method;
import android.media.AudioFormat;
import org.json.JSONObject;

import com.yqritc.scalablevideoview.ScalableType;
import com.yqritc.scalablevideoview.ScaleManager;
import com.yqritc.scalablevideoview.Size;
import android.graphics.Matrix;

//import com.yqritc.scalablevideoview.ScalableVideoView;

public class VideoFragment extends Fragment{
	private final String TAG = "VideoFragment";
	private SurfaceView				mSurfaceView	= null;
	private VideoView				mVideoView		= null;
	private TgmVideoView			mSVideoView		= null;
	//private MediaCodecView			mMediaCodecView	= null;
	private MediaSyncView			mMediaCodecView	= null;
	private RtspCodecView			mRtspCodecView	= null;
	private SurfaceHolder			mSurfaceHolder	= null;
	private Handler					mHandler		= new Handler();
	private CallbackContext			mStartCallbackContext	= null;

	private String mUrl = "rtsp://10.6.21.163/profile2/media.smp";
	private int mX = 0;
	private int mY = 0;
	private int mWidth = 1280;
	private int mHeight = 720;
	private int mRotation = 0;
	private int mVideoWidth = 1920;
	private int mVideoHeight = 1080;
	private float mDegree = 0;
	private float mExtDegree = 0;
	private float mMonitorCenterX = 0;
	private float mMonitorCenterY = 0;
	private boolean mIsMirrored = false;

//	private int mResolX = 1280;
//	private int mResolY = 720;
//	private int mBezelLeft = 0;
//	private int mBezelRight = 0;
//	private int mBezelTop = 0;
//	private int mBezelBottom = 0;

	private String mFitting = "stretch";

    protected static final String ASSETS = "/android_asset/";

	private RtspPlayer mRtspPlayer;
	private VideoFragmentListener listener;

    public interface VideoFragmentListener {
        void onFailed();
        void onClose();
        void onSuccess();
	}

	public RtspPlayer getRtspPlayer(){
		return mRtspPlayer;
	}


	/*
	private void setScaleAndRotation(VideoView vv){
		Log.d(TAG, "setScaleAndRotation: " + mX + "," + mY + "," + mWidth + "," + mHeight + "," + mResolX + "," + mResolY + "," + mRotation );
		vv.setScaleX(3.0f);
		vv.setScaleY(3.0f);
		vv.setPivotX(1.0f);
		vv.setPivotX(1.0f);
		vv.setRotation(45.0f);
	}
	*/

	/*
	private synchronized LinearLayout getScalableVideoViewLayout(LinearLayout main, String fitting){
		if(mSVideoView == null){
			mSVideoView = new TgmVideoView(getActivity());
		}

		ScalableType st = ScalableType.FIT_XY;

		if(fitting.equals("stretch")){
			st = ScalableType.FIT_XY;
		}else if(fitting.equals("fit")){
			st = ScalableType.FIT_CENTER;
		}else if(fitting.equals("crop")){
			st = ScalableType.CENTER_CROP;
		}else if(fitting.equals("fill")){
			st = ScalableType.FIT_XY;
		}else if(fitting.equals("contain")){
			st = ScalableType.FIT_CENTER;
		}else if(fitting.equals("cover")){
			st = ScalableType.CENTER_CROP;
		}

		try{
			mSVideoView.initializeMediaPlayer(mUrl, st, mStartCallbackContext);
		}catch(Exception e){
			Log.e(TAG, "Exception: " + e.getMessage());
		}

		mSVideoView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		
		main.addView(mSVideoView);
		return main;
	}
	*/

	/*
	private LinearLayout getVideoViewLayout(LinearLayout main){
		mVideoView = new VideoView(getActivity());

		//mVideoView.setVideoURI(Uri.parse(mUrl));
		mVideoView.setVideoUrl(mUrl);

		mVideoView.setClickable(false);
		mVideoView.setFocusable(false);
		//setScaleAndRotation(mVideoView);

		mVideoView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		//mVideoView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
		//mVideoView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT));
		//mVideoView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		
		main.addView(mVideoView);
		mSurfaceHolder = mVideoView.getHolder();

		mSurfaceHolder.setKeepScreenOn(true);
		mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
            }
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
				stopVideoPlayer();
            }
            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}
		});
		return main;
	}
	*/

	/*
	private LinearLayout getRtspLayout(LinearLayout main){

		mSurfaceView = new SurfaceView(getActivity());

		mSurfaceView.setClickable(false);
		mSurfaceView.setFocusable(false);
		mSurfaceView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		main.addView(mSurfaceView);
		mSurfaceHolder = mSurfaceView.getHolder();

		mSurfaceHolder.setKeepScreenOn(true);
		mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				Log.d(TAG, "surface created");
			}
			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				Log.d(TAG, "surface destroyed");
			}
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				Log.d(TAG, "surface changed: " + format + "," + width + "," + height);

				RtspPlayerListener listener = new RtspPlayerListener() {
					public void onFailed() {
						Log.e(TAG, "RtspPlayerListener onFailed");
						//VideoFragment.this.onDestroy();
						VideoFragment.this.listener.onFailed();
					}
					public void onClose() {
						Log.e(TAG, "RtspPlayerListener onClose");
						VideoFragment.this.listener.onClose();
					}
					public void onSuccess() {
						Log.e(TAG, "RtspPlayerListener onSuccess");
						VideoFragment.this.listener.onSuccess();
					}
				};

				startRtspPlayer(mSurfaceHolder.getSurface(), mUrl, width, height, listener);
			}
		});
		return main;
	}
	*/

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle saved) {

		///////////////////////
		// modify for android version 6, for viewz
		LinearLayout main = new LinearLayout(getActivity());

		/*
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(mWidth + mX, mHeight + mY);
		layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
		layoutParams.setMargins(mX, mY, 0, 0);
		main.setLayoutParams(layoutParams);
		
		//main.setPadding(mX, mY, 0, 0);
		*/

		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(mWidth, mHeight);
		layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
		layoutParams.setMargins(mX, mY, 0, 0);

		main.setLayoutParams(layoutParams);

		// mirror video
		if(this.mIsMirrored){
			main.setScaleX(-1f);
		}

		main.setRotation((float)mDegree - (float)mExtDegree);

		main.setClickable(false);
		main.setFocusable(false);


//mUrl = "https://usbucket.connectplayer.net/office.connectplayer.net/youtube-fc921b463e10e51442dee5e4fa3ffd14.mp4";

		if(mUrl.startsWith("rtsp://")){
			//return getRtspLayout(main);
			return getRtspLayoutTV(main);
		}else{
			//return getScalableVideoViewLayout(main, mFitting); // 23.0, 13.2, com.tgm.connect+ & 21.3, 1.6, surfaceflinger && 10.6, 0.9 media.codec hw/+ && 17.0, 0.6, android.hardwar+
			return getCodecLayoutTV(main);	// 60.0, 10.2, com.tgm.connect+ & 23.3, 1.6, surfaceflinger && 19.6, 0.9 media.codec hw/+ && 10.0, 0.6, android.hardwar+

			//return getCodecLayout(main);
			//return getVideoViewLayout(main);
		}
	}

	void applyDisplayOptions(JSONObject options){

		int computedX = 0, computedY = 0, computedWidth = 3840, computedHeight = 2160;
		float degree = 0;
		int resolX = 3840, resolY = 2160;
		int computedBL = 0, computedBR = 0, computedBT = 0, computedBB = 0;
		String fitting = "stretch";
		try {
			float xRes = Float.valueOf(options.getString("screenWidth"));
			float yRes = Float.valueOf(options.getString("screenHeight"));
			float width = Float.valueOf(options.getString("width"));
			float height = Float.valueOf(options.getString("height"));
			float top =  Float.valueOf(options.getString("top"));
			float left = Float.valueOf(options.getString("left"));
			float videoWidth = Float.valueOf(options.getString("videoWidth"));
			float videoHeight = Float.valueOf(options.getString("videoHeight"));
			degree = Float.valueOf(options.getString("degree"));
			fitting = options.getString("fitting");
			this.mIsMirrored = options.getBoolean("isMirrored");

			float extScreenWidth = Float.valueOf(options.getString("extScreenWidth"));
			float extScreenHeight = Float.valueOf(options.getString("extScreenHeight"));
			float extVisualWidth = Float.valueOf(options.getString("extVisualWidth"));
			float extVisualHeight = Float.valueOf(options.getString("extVisualHeight"));
			float extLeftTopX = Float.valueOf(options.getString("extLeftTopX"));
			float extLeftTopY = Float.valueOf(options.getString("extLeftTopY"));
			float extRotation = Float.valueOf(options.getString("extRotation"));

			float fullScrW = xRes * (extScreenWidth/extVisualWidth);
			float fullScrH = yRes * (extScreenHeight/extVisualHeight);

			float absLeft = fullScrW * left/100;
			float absTop = fullScrH * top/100;

			float absWidth = fullScrW * (width/100);
			float absHeight = fullScrH * (height/100);

			float relLeft = absLeft - fullScrW * extLeftTopX/extScreenWidth;
			float relTop = absTop -  fullScrH * extLeftTopY/extScreenHeight;

			// compute difference between monitor & screen
			float xx = ((extLeftTopX + extVisualWidth/2)/extScreenWidth) * fullScrW - (absLeft + absWidth/2);
			float yy = ((extLeftTopY + extVisualHeight/2)/extScreenHeight) * fullScrH - (absTop + absHeight/2);

			double xxDash = xx*Math.cos(Math.toRadians(-extRotation)) - yy*Math.sin(Math.toRadians(-extRotation));
			double yyDash = xx*Math.sin(Math.toRadians(-extRotation)) + yy*Math.cos(Math.toRadians(-extRotation));

			this.mX = (int)(relLeft - ((float)xxDash - xx));
			this.mY = (int)(relTop - ((float)yyDash - yy));

			this.mWidth = (int)absWidth;
			this.mHeight = (int)absHeight;
			this.mDegree = degree;
			this.mExtDegree = extRotation;
			this.mFitting = fitting;
			this.mVideoWidth = (int)videoWidth;
			this.mVideoHeight = (int)videoHeight;

			//Log.d(TAG, "VideoFragment setRect at (" + relLeft + ", " + relTop + ") with " + absWidth + "x" + absHeight + " within " + xRes + "x" + yRes + ", degree " + degree + ", fitting: " + fitting);
			Log.d(TAG, "VideoFragment setRect options: " + options.toString());
			Log.d(TAG, "VideoFragment setRect mX: " + mX + ", mY: " + mY + ", mWidth: " + mWidth + ", mHeight: " + mHeight + ", mDegree: " + mDegree + ", mExtDegree: " + mExtDegree + ", mFitting: " + mFitting + ", mVideoWidth: " + mVideoWidth + ", mVideoHeight: " + mVideoHeight);

		} catch (Exception e) {
		}

	}

	public void setUrl(String url){
		this.mUrl = url;
	}

	public void setCallbackContext(CallbackContext callbackContext) {
		this.mStartCallbackContext = callbackContext;
	}

	public void seekTo(int msec, long triggerTimeMS, CallbackContext callbackContext) throws Exception {
		Log.d(TAG, "VideoFragment seekTo " + msec + " at " + triggerTimeMS);
		if(mVideoView != null){
			mVideoView.seekTo(msec);
		}else if(mSVideoView != null){
			mSVideoView.newSeekTo(msec, triggerTimeMS, callbackContext);
		}else if(mMediaCodecView != null){
			mMediaCodecView.newSeekTo(msec, triggerTimeMS, callbackContext);
		}else{
			throw new Exception("VideoFragment seekTo failed");
		}
	}

	public void changeVideoSource(String url, CallbackContext callbackContext) throws Exception {
		Log.d(TAG, "VideoFragment change video source " + url);
		if(mVideoView != null){
			mVideoView.setVisibility(View.VISIBLE);
			mVideoView.setVideoUrl(url);
			callbackContext.success();
		}else if(mSVideoView != null){
			mSVideoView.changeVideoSource(url, callbackContext);
			//mSVideoView.show();
		}else if(mMediaCodecView != null){
			mMediaCodecView.changeVideoSource(url, callbackContext);
		}else{
			throw new Exception("changeVideoSource failed");
		}
	}

	public void hide(CallbackContext callbackContext) {
		Log.d(TAG, "VideoFragment try to hide:");
		if(mVideoView != null){
			mVideoView.setVisibility(View.INVISIBLE);
			callbackContext.success("hide() in VideoView is not supported");
		}else if(mSVideoView != null){
			mSVideoView.hide();
			callbackContext.success("hide(), success");
		}else if(mMediaCodecView != null){
			mMediaCodecView.hide();
			callbackContext.success("hide(), success");
		}else{
			callbackContext.success("No instance for hide(), anyway, success");
		}
	}

	public void show(CallbackContext callbackContext) {
		Log.d(TAG, "VideoFragment try to show:");
		if(mVideoView != null){
			mVideoView.setVisibility(View.VISIBLE);
			callbackContext.success("show() in VideoView.");
		}else if(mSVideoView != null){
			mSVideoView.show();
			callbackContext.success("show(), success");
		}else if(mMediaCodecView != null){
			mMediaCodecView.show();
			callbackContext.success("show(), success");
		}else{
			callbackContext.success("No instance for show(), anyway, success");
		}
	}

	public void gotoStartingPoint(CallbackContext callbackContext) throws Exception {
		Log.d(TAG, "VideoFragment try to gotoStartingPoint:");
		if(mVideoView != null){
			mVideoView.seekTo(0);
			callbackContext.success("gotoStartingPoint() OK.");
		}else if(mSVideoView != null){
			mSVideoView.seekToAndPause(0);
			callbackContext.success("gotoStartingPoint(), success");
		}else if(mMediaCodecView != null){
			callbackContext.success("gotoStartingPoint(), not implemented");
		}else{
			callbackContext.success("No instance for gotoStartingPoint(), anyway, success");
		}
	}


	public void setListener(VideoFragmentListener listener){
		this.listener = listener;
	}

	private void startVideoPlayer(){}

	private synchronized void stopVideoPlayer(){
		if(mVideoView != null){
			mVideoView.stopPlayback();
			mVideoView = null;
		}else if(mSVideoView != null){
			//mSVideoView.stop();
			//mSVideoView.release();
			mSVideoView.stopPlayback();
			mSVideoView = null;
		}
	}

	private void startRtspPlayer(Surface surface, String url, int width, int height, RtspPlayerListener listener){
		if(mRtspPlayer != null){
			Log.v(TAG, "RtspPlayer already exists");
			return;
		}
		Log.v(TAG, "start RtspPlayer");
		mRtspPlayer = new RtspPlayer();
		mRtspPlayer.start(surface, url, width, height, listener);
	}

	private void stopRtspPlayer(){
		if(mRtspPlayer != null){
			Log.v(TAG, "stop RtspPlayer");
			mRtspPlayer.stop();
			mRtspPlayer = null;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.v(TAG, "onResume");
	}

	public void stopPlayers(){
		stopRtspPlayer();
		stopVideoPlayer();
		stopCodecPlayer();
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.v(TAG, "onPause");
	}

	@Override
	public void onDestroy() {
		stopPlayers();
		super.onDestroy();
		Log.v(TAG, "onDestroy");
	}

	///////////////////////////////////////////////////////////////////////////////
	// MediaCodec based player.
	///////////////////////////////////////////////////////////////////////////////

	private void startCodecPlayer(){}
	private void stopCodecPlayer(){
		if(mMediaCodecView != null){
			mMediaCodecView.stopPB();
			mMediaCodecView = null;
		}
	}

	private LinearLayout getCodecLayoutTV(LinearLayout main){
		if(mMediaCodecView == null){
			Log.d(TAG, "getCodecLayoutTV, try to create new MediaCodec");
			//mMediaCodecView = new MediaCodecView(getActivity());
			mMediaCodecView = new MediaSyncView(getActivity());
		}

		try{
			mMediaCodecView.initialize(mUrl, mStartCallbackContext);
		}catch(Exception e){
			Log.e(TAG, "Exception: " + e.getMessage());
		}

		mMediaCodecView.setClickable(false);
		mMediaCodecView.setFocusable(false);
		mMediaCodecView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		//mSurfaceView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));


		main.addView(mMediaCodecView);
		return main;

		/*
		TextureView tv = new TextureView(getActivity());

		tv.setClickable(false);
		tv.setFocusable(false);
		tv.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		//mSurfaceView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		main.addView(tv);

		tv.setSurfaceTextureListener(new TextureView.SurfaceTextureListener(){
			@Override
			public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height){
				Log.d(TAG, "onSurfaceTextureAvailable");
				try {
					Surface surface = new Surface(surfaceTexture);
					mMediaCodecView.startPB(surface, mUrl);
				} catch (Exception e) {
					Log.d(TAG, "play error: " + e.getLocalizedMessage());
					PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getLocalizedMessage());
					result.setKeepCallback(false); // release status callback in JS side
				}
			}

			@Override
			public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height){
				Log.d(TAG, "onSurfaceTextureSizeChanged");
			}

			@Override
			public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture){
				Log.d(TAG, "onSurfaceTextureDestroyed");
				return false;
			}

			@Override
			public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture){
				//Log.d(TAG, "onSurfaceTextureUpdated");
			}
		});
	
		return main;
		*/
	}

	class RtspCodecView extends TextureView implements TextureView.SurfaceTextureListener{
		protected String TAG = "VideoFragment::RtspCodecView";
		private Surface mSurface	= null;

		public RtspCodecView(Context context){
			super(context, null, 0);
			setSurfaceTextureListener(this);
		}

		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height){
			Log.d(TAG, "onSurfaceTextureAvailable");
			try {
				mSurface = new Surface(surfaceTexture);

				RtspPlayerListener listener = new RtspPlayerListener() {
					public void onFailed() {
						Log.e(TAG, "RtspPlayerListener onFailed");
						//VideoFragment.this.onDestroy();
						VideoFragment.this.listener.onFailed();
					}
					public void onClose() {
						Log.e(TAG, "RtspPlayerListener onClose");
						VideoFragment.this.listener.onClose();
					}
					public void onSuccess() {
						Log.e(TAG, "RtspPlayerListener onSuccess");
						VideoFragment.this.listener.onSuccess();
					}
				};
				startRtspPlayer(mSurface, mUrl, width, height, listener);
			} catch (Exception e) {
				Log.d(TAG, "play error: " + e.getLocalizedMessage());
				PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getLocalizedMessage());
				result.setKeepCallback(false); // release status callback in JS side
			}
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
			Log.d(TAG, "onSurfaceTextureSizeChanged");
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			Log.d(TAG, "onSurfaceTextureDestroyed");
			stopRtspPlayer();
			return false;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
			//Log.d(TAG, "onSurfaceTextureUpdated");
		}

		/*
		void initialize(String url, CallbackContext callbackContext){
			Log.d(TAG, "initialize");
			long then = System.currentTimeMillis();
			stopRtspPlayer();

			class CheckAndInitialize implements Runnable{
				@Override
				public void run(){
					if(mSurface == null || mVDecThread != null || mADecThread != null){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							Log.d(TAG, "surface is not ready");
							callbackContext.error("surface is not ready");
							return;
						}
						mHandler.postDelayed(this, 100);
					}else{
						startRtspPlayer(mSurface, mUrl, width, height, listener);
					}
				}
			}
			mHandler.post(new CheckAndInitialize());
		}
		*/
	}

	private LinearLayout getRtspLayoutTV(LinearLayout main){

		if(mRtspCodecView == null){
			Log.d(TAG, "try to create new mRtspCodecView");
			mRtspCodecView = new RtspCodecView(getActivity());
		}

		Log.d(TAG, "getRtspLayoutTV, try to initialize mRtspCodecView, mRtspCodecView: " + mRtspCodecView);

		/*
		try{
			mRtspCodecView.initialize(mUrl, mStartCallbackContext);
		}catch(Exception e){
			Log.e(TAG, "Exception: " + e.getMessage());
		}
		*/

		mRtspCodecView.setClickable(false);
		mRtspCodecView.setFocusable(false);
		mRtspCodecView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		main.addView(mRtspCodecView);
		return main;

		/*
		mSurfaceView = new SurfaceView(getActivity());

		mSurfaceView.setClickable(false);
		mSurfaceView.setFocusable(false);
		mSurfaceView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

		main.addView(mSurfaceView);
		mSurfaceHolder = mSurfaceView.getHolder();

		mSurfaceHolder.setKeepScreenOn(true);
		mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				Log.d(TAG, "surface created");
			}
			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				Log.d(TAG, "surface destroyed");
			}
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				Log.d(TAG, "surface changed: " + format + "," + width + "," + height);

				RtspPlayerListener listener = new RtspPlayerListener() {
					public void onFailed() {
						Log.e(TAG, "RtspPlayerListener onFailed");
						//VideoFragment.this.onDestroy();
						VideoFragment.this.listener.onFailed();
					}
					public void onClose() {
						Log.e(TAG, "RtspPlayerListener onClose");
						VideoFragment.this.listener.onClose();
					}
					public void onSuccess() {
						Log.e(TAG, "RtspPlayerListener onSuccess");
						VideoFragment.this.listener.onSuccess();
					}
				};

				startRtspPlayer(mSurfaceHolder.getSurface(), mUrl, width, height, listener);
			}
		});
		return main;
		*/
	}



	/*
	private LinearLayout getCodecLayout(LinearLayout main){

		mSurfaceView = new SurfaceView(getActivity());

		mSurfaceView.setClickable(false);
		mSurfaceView.setFocusable(false);
		mSurfaceView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		//mSurfaceView.setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));

		main.addView(mSurfaceView);
		mSurfaceHolder = mSurfaceView.getHolder();


		mSurfaceHolder.setKeepScreenOn(true);
		mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				Log.d(TAG, "surface created");
			}
			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				Log.d(TAG, "surface destroyed");
				//mSurfaceHolder = null;
				//mSurfaceView = null;
				//mVideoView = null;
			}
			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				Log.d(TAG, "surface changed: " + format + "," + width + "," + height);
				try {
					mMediaCodecView.startPB(holder.getSurface(), mUrl);
				} catch (Exception e) {
					Log.d(TAG, "play error: " + e.getLocalizedMessage());
					PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getLocalizedMessage());
					result.setKeepCallback(false); // release status callback in JS side
					//callbackContext.sendPluginResult(result);
					//callbackContext = null;
				}
			}
		});
	
		return main;
	}
	*/

	class AVCodecView extends TextureView implements TextureView.SurfaceTextureListener{
		protected String TAG = "VideoFragment::AVCodecView";
		private AudioTrack mAudioTrack = null;
		private Method mGetAudioLatencyMethod;
		private static final long MAX_LATENCY_MS = 5000;

		public AVCodecView(Context context){
			super(context, null, 0);
			setSurfaceTextureListener(this);
			try {
				mAudioTrack = new AudioTrack(
						AudioManager.STREAM_MUSIC,
						44100,
						2,
						AudioFormat.ENCODING_PCM_16BIT,
						16*1024,
						AudioTrack.MODE_STREAM);

				mGetAudioLatencyMethod = android.media.AudioTrack.class.getMethod("getLatency", (Class<?>[]) null);
			} catch (NoSuchMethodException e) {
				Log.d(this.TAG, "no such a method: getlatency");
			}
		}

		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height){
			Log.d(TAG, "onSurfaceTextureAvailable");
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
			Log.d(TAG, "onSurfaceTextureSizeChanged");
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			Log.d(TAG, "onSurfaceTextureDestroyed");
			return false;
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
			//Log.d(TAG, "onSurfaceTextureUpdated");
		}

		protected long checkAudioLatency(){
			final long latency = getAudioLatency();
			Log.v(TAG, "current audio latency: " + latency);
			if(latency > MAX_LATENCY_MS){
				Log.e(TAG, "restart screen for too large audio latency: " + latency);
				restartScreen();
				return 0;
			}
			return latency;
		}

		protected long getAudioLatency(){
			try{
				if(mAudioTrack != null && mGetAudioLatencyMethod != null){
					long latency = (Integer) mGetAudioLatencyMethod.invoke(mAudioTrack, (Object[]) null);
					latency = Math.max(latency, 0);
					return latency;
				}
			}catch(Exception e){
				e.printStackTrace();
			}

			return 0;
		}

		private void restartScreen(){
			return;
		}
	}

	class MediaCodecView extends AVCodecView{
		protected String TAG = "VideoFragment::MediaCodecView";
		protected TgmAudioDecodeThread mADecThread	= null;
		private TgmVideoDecodeThread mVDecThread	= null;
		private Surface mSurface	= null;
		public MediaCodecView(Context context){
			super(context);
		}

		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height){
			Log.d(TAG, "onSurfaceTextureAvailable");
			try {
				mSurface = new Surface(surfaceTexture);
			} catch (Exception e) {
				Log.d(TAG, "play error: " + e.getLocalizedMessage());
				PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getLocalizedMessage());
				result.setKeepCallback(false); // release status callback in JS side
			}
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			Log.d(TAG, "onSurfaceTextureDestroyed");
			stopPB();
			mSurface = null;
			return false;
		}

		void initialize(String url, CallbackContext callbackContext){
			Log.d(TAG, "initialize");
			long then = System.currentTimeMillis();
			stopPB();

			class CheckAndInitialize implements Runnable{
				@Override
				public void run(){
					if(mSurface == null || mVDecThread != null || mADecThread != null){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							Log.d(TAG, "surface is not ready");
							callbackContext.error("surface is not ready");
							return;
						}
						mHandler.postDelayed(this, 100);
					}else{
						startPB(mSurface, url, callbackContext);
					}
				}
			}
			mHandler.post(new CheckAndInitialize());
		}


		public void changeVideoSource(String url, CallbackContext callbackContext){
			stopPB();

			mHandler.post(new Runnable() {
				@Override
				public void run() {
					if(mVDecThread == null && mADecThread == null){
						startPB(mSurface, url, callbackContext);
					}else{
						mHandler.postDelayed(this, 100);
					}
				}
			});
		}

		public void hide(){
			setVisibility(View.INVISIBLE);
		}
		
		public void show(){
			setVisibility(View.VISIBLE);
		}

		public void newSeekTo(int msec, long triggerTimeMS, CallbackContext callbackContext){
			Log.d(TAG, "VideoFragment seekTo to mediacodec " + msec + " at " + triggerTimeMS + ", mVDecThread: " + mVDecThread + ", mADecThread: " + mADecThread);
			//if(mVDecThread != null || mADecThread != null){
			if(mVDecThread != null){
				mVDecThread.setStartMS(triggerTimeMS);
				mVDecThread.seekTo(msec);
				if(mADecThread != null){
					mADecThread.setStartMS(triggerTimeMS);
					mADecThread.seekTo(msec);
				}
			}else{
				callbackContext.error("MediaCodec seekTo failed, mVDecThread is null");
			}
			callbackContext.success();
		}

		public final void startPB(Surface surface, String path, CallbackContext callbackContext){
			Log.v(TAG, "startPB");

			mVDecThread = new TgmVideoDecodeThread(surface, path, 0, -1, new TgmVideoDecodeThread.PlayCompleteListener() {
				@Override
				public void playComplete() {
					Log.v(TAG, "playback complete");
				}
			});
			mVDecThread.start();

			long then = System.currentTimeMillis();
			/*
			while (mVDecThread.started == false) {
				try {
					long now = System.currentTimeMillis();
					if(now - then > 5000){
						Log.d(TAG, "startPB timeout");
						stopPB();
						callbackContext.error("startPB timeout");
						return;
					}
					Thread.sleep(100);
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
			*/

			mHandler.post(new Runnable() {
				@Override
				public void run() {
					if(mVDecThread == null){
						callbackContext.error("startPB failed, mVDecThread is null");
						return;
					}
					if(!mVDecThread.started){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							Log.d(TAG, "startPB timeout");
							stopPB();
							callbackContext.error("startPB timeout");
							return;
						}
						mHandler.postDelayed(this, 100);
					}else{
						Log.d(TAG, "startPB video success");
						show();
						callbackContext.success("start play: OK");
					}
				}
			});

			//long skipMs = checkAudioLatency();

			//mADecThread.setPriority(10);
			mADecThread = new TgmAudioDecodeThread(path, System.currentTimeMillis(), 0, -1);
			mADecThread.start();

			while(mADecThread != null && !mADecThread.isAlive()){
				try{
					Thread.sleep(100);
				}catch(InterruptedException ex){
					ex.printStackTrace();
				}
			}
		}

		public final synchronized void stopPB(){
			Log.v(TAG, "stopPB");

			if(mVDecThread == null && mADecThread == null){
				Log.e(TAG, "stopMediaCodec: redundant action");
				return;
			}

			if (mVDecThread != null) {
				if (mVDecThread.isAlive()) {
					mVDecThread.stopThread();
				}
			}
			if (mADecThread != null) {
				if (mADecThread.isAlive()) {
					mADecThread.stopThread();
				}
			}

			/*
			long then = System.currentTimeMillis();
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					if(mVDecThread != null && mVDecThread.isAlive()){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							mVDecThread.releaseResource();
							Log.d(TAG, "stopPB video timeout");
							return;
						}
						mHandler.postDelayed(this, 100);
					}else{
						Log.d(TAG, "stopPB video success");
						mVDecThread = null;
					}
				}
			});

			mHandler.post(new Runnable() {
				@Override
				public void run() {
					if(mADecThread != null && mADecThread.isAlive()){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							mADecThread.releaseResource();
							Log.d(TAG, "stopPB audio timeout");
							return;
						}
						mHandler.postDelayed(this, 100);
					}else{
						Log.d(TAG, "stopPB audio success");
						mADecThread = null;
					}
				}
			});
			*/

			int timeout = 50;
			if (mVDecThread != null) {
				while (mVDecThread.isAlive()) {
					try {
						Thread.sleep(200);
						//Thread.yield();
						timeout--;
					//} catch (InterruptedException ex) {
					} catch (Exception ex) {
					}

					if (timeout < 0) {
						Log.e(TAG, "stopMediaCodec, video release timeout");
						mVDecThread.releaseResource();
						break;
					}
				}
			}
			mVDecThread = null;

			timeout = 50;
			if (mADecThread != null) {
				while (mADecThread.isAlive()) {
					try {
						Thread.sleep(200);
						//Thread.yield();
						timeout--;
					//} catch (InterruptedException ex) {
					} catch (Exception ex) {
					}

					if (timeout < 0) {
						Log.e(TAG, "stopMediaCodec, video release timeout");
						mADecThread.releaseResource();
						break;
					}
				}
			}
			mADecThread = null;
		}
	}

	class MediaSyncView extends TextureView implements TextureView.SurfaceTextureListener{
		protected String TAG = "VideoFragment::MediaSyncView";
		private TgmMediaSyncThread mMediaSyncThread	= null;
		private Surface mSurface	= null;
		public MediaSyncView(Context context){
			super(context, null, 0);
			setSurfaceTextureListener(this);
		}

		@Override
		public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height){
			Log.d(TAG, "onSurfaceTextureAvailable, width: " + width + ", height: " + height);
			try {
				mSurface = new Surface(surfaceTexture);

				Size viewSize = new Size(width, height);
				Size videoSize = new Size(mVideoWidth, mVideoHeight);
				ScaleManager scaleManager = new ScaleManager(viewSize, videoSize);
				ScalableType stype = ScalableType.FIT_CENTER;
				if(mFitting.equals("cover")){
					stype = ScalableType.CENTER_CROP;
				}else if(mFitting.equals("fill")){
					stype = ScalableType.FIT_XY;
				}else if(mFitting.equals("contain")){
					stype = ScalableType.FIT_CENTER;
				}
				Log.d(TAG, "onSurfaceTextureAvailable, setTransform: " + stype);
				Matrix matrix = scaleManager.getScaleMatrix(stype);
				setTransform(matrix);

				//clearSurface();
			} catch (Exception e) {
				Log.d(TAG, "play error: " + e.getLocalizedMessage());
				PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getLocalizedMessage());
				result.setKeepCallback(false); // release status callback in JS side
			}
		}

		@Override
		public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
			Log.d(TAG, "onSurfaceTextureDestroyed");
			stopPB();
			mSurface = null;
			return false;
		}

		@Override
		public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
			Log.d(TAG, "onSurfaceTextureSizeChanged");
		}

		@Override
		public void onSurfaceTextureUpdated(SurfaceTexture surface) {
			//Log.d(TAG, "onSurfaceTextureUpdated");
		}

		private boolean mIsIntializing = false;
		private boolean mIsReady = true;
		public void initialize(String url, CallbackContext callbackContext){
			Log.d(TAG, "initialize() start");

			long then = System.currentTimeMillis();
			mIsReady = true;

			class CheckAndInitialize implements Runnable{
				@Override
				public void run(){
					/*
					int retryCount = 0;
					while(mIsReady && mIsIntializing && retryCount++ < 50){
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
						}
					}
					*/

					if(mIsReady && mIsIntializing){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							Log.d(TAG, "initialize() fail, timeout in ready state");
							callbackContext.error("initialize() fail, timeout in ready state");
							return;
						}
						mHandler.postDelayed(this, 100);
					}

					mIsReady = false;
					mIsIntializing = true;
					if(mSurface == null){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							Log.d(TAG, "initialize() fail, surface is not ready");
							callbackContext.error("initialize() fail, surface is not ready");
							mIsIntializing = false;
							return;
						}
						mHandler.postDelayed(this, 100);
					}else{
						//clearSurface();
						stopPB();
						startPB(mSurface, url, callbackContext);
						Log.d(TAG, "initialize() done");
						mIsIntializing = false;
						return;
					}
				}
			}
			mHandler.post(new CheckAndInitialize());
		}

		public void changeVideoSource(String url, CallbackContext callbackContext){
			initialize(url, callbackContext);
		}

		public void hide(){
			setVisibility(View.INVISIBLE);
			if(mMediaSyncThread != null){
				mMediaSyncThread.hide();
			}
		}
		
		public void show(){
			setVisibility(View.VISIBLE);
			if(mMediaSyncThread != null){
				mMediaSyncThread.show();
			}
		}

		public void newSeekTo(int msec, long triggerTimeMS, CallbackContext callbackContext){
			Log.d(TAG, "VideoFragment seekTo to mediacodec " + msec + " at " + triggerTimeMS + ", mMediaSyncThread: " + mMediaSyncThread);
			if(mMediaSyncThread != null && mMediaSyncThread.isStarted()){
				mMediaSyncThread.setStartMS(triggerTimeMS);
				mMediaSyncThread.seekTo(msec);
			}else{
				callbackContext.error("MediaCodec seekTo failed, mMediaSyncThread is null");
			}
			callbackContext.success();
		}

		private boolean mStartToRender = false;
		public final void startPB(Surface surface, String path, CallbackContext callbackContext){
			Log.v(TAG, "startPB() start");
			mStartToRender = false;
			mMediaSyncThread = new TgmMediaSyncThread(surface, path, new TgmMediaSyncThread.StartRenderListener(){
				@Override
				public void onStartRender(){
					mStartToRender = true;
					Log.d(TAG, "VideoFragment onStartRender");
				}
			});
	
			mMediaSyncThread.start();

			long then = System.currentTimeMillis();

			mHandler.post(new Runnable() {
				@Override
				public void run() {
					if(mMediaSyncThread == null){
						Log.d(TAG, "startPB() failed with null mMediaSyncThread");
						callbackContext.error("startPB() done, failed, mMediaSyncThread is null");
						return;
					}
					if(!mMediaSyncThread.isStarted()){
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							Log.d(TAG, "startPB() done, failed with timeout");
							stopPB();
							callbackContext.error("startPB timeout");
							return;
						}
						mHandler.postDelayed(this, 100);
					}else{
						Log.d(TAG, "startPB() done, success");
						callbackContext.success("start play: OK");
					}
				}
			});

			mHandler.post(new Runnable(){
				@Override
				public void run(){
					//if(mMediaSyncThread != null && mMediaSyncThread.mVideoSyncThread != null && mMediaSyncThread.mVideoSyncThread.isStartToShow()){
					if(mStartToRender){
						show();
						return;
					}else{
						long now = System.currentTimeMillis();
						if(now - then > 5000){
							Log.d(TAG, "waiting for isStartToShow, failed with timeout");
							show();
							return;
						}
						mHandler.postDelayed(this, 100);
					}
				}
			});
		}

		public final synchronized void stopPB(){
			Log.v(TAG, "stopPB() start");

			hide();
			if(mMediaSyncThread == null){
				Log.d(TAG, "stopPB() done with null MediaSyncThread");
				return;
			}

			if(mMediaSyncThread.isAlive()){
				mMediaSyncThread.interrupt();
				//mMediaSyncThread.stopThread();
			}

			// wait for thread to exit
			if(mMediaSyncThread.isAlive()){
				try {
					mMediaSyncThread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			mMediaSyncThread = null;
			Log.v(TAG, "stopPB() done");
		}

		private void clearSurface(){

			Log.d(TAG, "clearSurface");

			if(isAvailable()){
				Canvas canvas = lockCanvas();
				if(canvas != null){
					//canvas.drawColor(Color.BLACK, PorterDuff.Mode.CLEAR);
					canvas.drawColor(Color.RED);
					unlockCanvasAndPost(canvas);
				}
			}
		}
	}
}


