package com.tgm.cordova.videoplayer;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Fragment;

import android.util.DisplayMetrics;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.widget.VideoView;
import android.graphics.Color;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaResourceApi;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface; 
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.ComponentName;
import android.content.Context;
import android.util.AttributeSet;

import java.io.FileInputStream;
import java.io.DataInputStream;
import android.media.AudioTrack;
import android.media.AudioFormat;
import java.io.IOException;
import android.view.View;
import java.util.Timer;
import java.util.TimerTask;
import java.util.List;
import java.util.ArrayList;
import java.lang.Thread;

import android.app.Application;
import android.content.res.Resources;
import android.support.v4.view.ViewCompat;
//import androidx.core.view.ViewCompat

public class VideoPlayerSingle extends CordovaPlugin{
	protected String TAG = "TgmVideoPlayer";

	private String mUrl = null;
	//private boolean					isFinish				= false;
	private Handler	mHandler = new Handler();
	private VideoFragment rtspFragment = null;
	private VideoFragment videoFragment = null;
	private FrameLayout videoView = null;
	private FrameLayout rtspView = null;
	private CallbackContext rtspDisconnectionCallbackContext = null;		// need global callbackContext to check rtsp diconnection event

	private int videoViewId = 1013; //<- set to random number to prevent conflict with other plugins
	private int rtspViewId = videoViewId + 1000;
	private int camViewId = rtspViewId + 1000;

	//private CordovaWebView webView;
	public void close(){
		FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		if(rtspFragment != null){
			fragmentTransaction.remove(rtspFragment);
			try {
				fragmentTransaction.commit();
			}catch(Exception e){
				Log.e(TAG, "RtspFragment transaction commit failed at stop");
			}
			rtspFragment = null;
			rtspView = null;
		}

		if(videoFragment != null){
			videoFragment.stopPlayers();
			fragmentTransaction.remove(videoFragment);
			try {
				fragmentTransaction.commit();
			}catch(Exception e){
				Log.e(TAG, "VideoFragment transaction commit failed at stop");
			}

			videoFragment = null;
			videoView = null;
		}
	}

	@Override
	public void initialize(final CordovaInterface cordova, CordovaWebView webView) {
		Log.d(this.TAG, "VideoPlayer plugin initialized...");
		super.initialize(cordova, webView);
		//this.webView = webView;
		webView.getView().setBackgroundColor(Color.TRANSPARENT);
		//webView.getView().setBackgroundColor(0x00444444);
		//webView.getView().setLayerType(WebView.LAYER_TYPE_HARDWARE, null);
		//webView.getView().setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
		//webView.getView().setBackgroundColor(Color.RED);
	}

	@Override
	public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
		Log.d(TAG, "action: " + action);

		if (action.startsWith("play")) {
			//PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
			//pluginResult.setKeepCallback(true);
			//callbackContext.sendPluginResult(pluginResult);

			CordovaResourceApi resourceApi = webView.getResourceApi();
			String target = args.getString(0);
			final JSONObject options = args.getJSONObject(1);
			Log.d(TAG, "try to start playing: " + target + ", options: " + options.toString());
			
			String fileUriStr;
			try {
				Uri targetUri = resourceApi.remapUri(Uri.parse(target));
				fileUriStr = targetUri.toString();
			} catch (IllegalArgumentException e) {
				fileUriStr = target;
			}

			Log.v(TAG, fileUriStr);

			final String path = stripFileProtocol(fileUriStr);

			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					if(action.equals("playRtspPlayer")){
						startPlayWithNewRtspPlayer(path, options, callbackContext);
					}else{
						startPlayWithNewVideoPlayer(path, options, callbackContext);
					}
				}
			});
			return true;
		} else if (action.startsWith("close")) {
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					if(action.equals("closeRtspPlayer")){
						stopPlayWithNewRtspPlayer(callbackContext);
					}else{
						stopPlayWithNewVideoPlayer(callbackContext);
					}
				}
			});

			return true;
		}else if(action.equals("seekTo")){

			int msec = args.getInt(0);
			long triggerTimeMS = args.getLong(1);

			if(videoFragment != null){
				cordova.getActivity().runOnUiThread(new Runnable() {
					public void run() {
						try{
							videoFragment.seekTo(msec, triggerTimeMS, callbackContext);
						}catch(Exception e){
							callbackContext.error(e.getMessage());
						}
					}
				});
			}else{
				callbackContext.error("videoFragment is null");
			}
			return true;
		}else if(action.equals("isRtspDisconnected")){
			PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
			pluginResult.setKeepCallback(true);
			callbackContext.sendPluginResult(pluginResult);
			rtspDisconnectionCallbackContext = callbackContext;

			Log.d(TAG, "start check rtsp connection.");
			return true;
		}else if(action.equals("changeVideoSource")){
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.changeVideoSource(args.getString(0), callbackContext);
						}else{
							callbackContext.error("videoFragment is null");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
				}
			});
			return true;
		}else if(action.equals("hide")){
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.hide(callbackContext);
						}else{
							callbackContext.success("videoFragment is null, anyway, hide success");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
				}
			});
			return true;
		}else if(action.equals("show")){
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.show(callbackContext);
						}else{
							callbackContext.success("videoFragment is null, anyway, show success");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
				}
			});
			return true;
		}else if(action.equals("gotoStartingPoint")){
			cordova.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.gotoStartingPoint(callbackContext);
						}else{
							callbackContext.success("videoFragment is null, anyway, gotoStartingPoint success");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
				}
			});
			return true;
		}

/*
		else if(action.equals("snapshot")){	// RTSP only, under developing
			if (callbackContext != null) {

				if(rtspFragment == null || rtspFragment.getRtspPlayer() == null || rtspFragment.getRtspPlayer().getVideoDecodeThread() == null){
					callbackContext.error("maybe null rtsp player...");
				}else{
					try{
						rtspFragment.getRtspPlayer().getVideoDecodeThread().tryToGetCurrentPicture(callbackContext);
					}catch(Exception e){
						callbackContext.error(e.getLocalizedMessage());
					}
				}
			}
			return true;
		}
*/

		return false;
	}

	public static String stripFileProtocol(String uriString) {
		if (uriString.startsWith("file://")) {
			return Uri.parse(uriString).getPath();
		}
		return uriString;
	}

	///////////////////////
	//  new 
	//////////////////////

	private void __setLayerOfViews(){
		List<View> views = new ArrayList<>();

		FrameLayout videoView = (FrameLayout) cordova.getActivity().findViewById(videoViewId);
		FrameLayout rtspView = (FrameLayout) cordova.getActivity().findViewById(rtspViewId);
		FrameLayout camView = (FrameLayout) cordova.getActivity().findViewById(camViewId);

		views.add((View)videoView);
		views.add((View)webView.getView());
		views.add((View)rtspView);
		views.add((View)camView);

		int depth = 0;

		for(View v : views){
			if(v == null) continue;

			//Log.d(TAG, "set depth of view, bringToFront: " + v);
			//v.bringToFront();
			//v.invalidate();

			depth += 100;
			ViewCompat.setTranslationZ(v, depth);
			Log.d(TAG, "set depth of view: " + depth + " : " + v);
		}
	}

	private void setLayerOfViews(){
		return;
		/*
		cordova.getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				new Handler().postDelayed(new Runnable(){
					@Override
					public void run(){
						__setLayerOfViews();
					}
				}, 1000);
			}
		});
		*/
	}

	synchronized void stopPlayWithNewRtspPlayer(CallbackContext callbackContext){
		if(rtspFragment == null){
			callbackContext.success("not RTSP Player running");
			return;
		}
		FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.remove(rtspFragment);
		fragmentTransaction.commit();
		rtspFragment = null;
		rtspView = null;

		callbackContext.success("stopRtspPlayer OK");
	}

	synchronized void startPlayWithNewRtspPlayer(String path, JSONObject options, CallbackContext callbackContext){

		if (rtspFragment != null) {
			callbackContext.error("Camera already started");
			return;
		}
		rtspFragment = new VideoFragment();

		rtspFragment.setListener(new VideoFragment.VideoFragmentListener(){
			public void onFailed() {
				Log.d(TAG, "callback VideoFragment rtsp onFailed");
				callbackContext.error("RTSP Action onFailed");
				if(VideoPlayerSingle.this.rtspDisconnectionCallbackContext != null){
					VideoPlayerSingle.this.rtspDisconnectionCallbackContext.error("RTSP Action onFailed & onClose");
					VideoPlayerSingle.this.rtspDisconnectionCallbackContext = null;
				}
			}
			public void onClose() {
				Log.d(TAG, "callback VideoFragment rtsp onClose");
				//callbackContext.error("RTSP Action onClose");
				if(VideoPlayerSingle.this.rtspDisconnectionCallbackContext != null){
					VideoPlayerSingle.this.rtspDisconnectionCallbackContext.error("RTSP Action onClose");
					VideoPlayerSingle.this.rtspDisconnectionCallbackContext = null;
				}
			}
			public void onSuccess() {
				Log.d(TAG, "callback VideoFragment rtsp onSuccess");
				callbackContext.success("VideoFragment success");
			}
		});

		rtspFragment.applyDisplayOptions(options);
		rtspFragment.setUrl(path);

		FrameLayout containerView = (FrameLayout)cordova.getActivity().findViewById(rtspViewId);
		if(containerView == null){
			Log.d(TAG, "null rtsp containerView, create new one with ViewId: " + rtspViewId);
			final FrameLayout newContainerView = new FrameLayout(cordova.getActivity().getApplicationContext());
			newContainerView.setId(rtspViewId);
			FrameLayout.LayoutParams containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
			cordova.getActivity().addContentView(newContainerView, containerLayoutParams);
			containerView = newContainerView;
		}

		rtspView = containerView;
		setLayerOfViews();

		//add the fragment to the container
		FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		fragmentTransaction.add(containerView.getId(), rtspFragment);
		//fragmentTransaction.commit();

		try {
			fragmentTransaction.commit();
			Log.d(TAG, "startPlayWithNewRtspPlayer complete. OK");
		}catch(Exception e){
			Log.e(TAG, "RtspFragment transaction commit failed");

			if(rtspFragment == null){
				callbackContext.error("RtspFragment transaction creation error");
				return;
			}
			fragmentTransaction.remove(rtspFragment);
			rtspFragment = null;
			callbackContext.error("RtspFragment transaction error");
		}
		return;
	}

	synchronized void stopPlayWithNewVideoPlayer(CallbackContext callbackContext){
		if(videoFragment == null){
			callbackContext.success("not Video Player running");
			return;
		}
		FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		videoFragment.stopPlayers();
		fragmentTransaction.remove(videoFragment);

		try {
			fragmentTransaction.commit();
		}catch(Exception e){
			Log.e(TAG, "VideoFragment transaction commit failed at stop");
		}

		videoFragment = null;
		videoView = null;

		callbackContext.success("stopVideoPlayer OK");
	}

	synchronized void startPlayWithNewVideoPlayer(String path, JSONObject options, CallbackContext callbackContext){

		if (videoFragment != null) {
			callbackContext.error("Video already started");
			return;
		}
		videoFragment = new VideoFragment();

		videoFragment.setListener(new VideoFragment.VideoFragmentListener(){
			public void onFailed() {
				Log.e(TAG, "VideoFragment onFailed");
				callbackContext.error("VideoFragment failed");
			}
			public void onClose() {
				Log.d(TAG, "VideoFragment onClose");
				callbackContext.error("VideoFragment close");
			}
			public void onSuccess() {
				Log.d(TAG, "VideoFragment onSuccess");
//callbackContext.success("VideoFragment success");
			}
		});

		videoFragment.applyDisplayOptions(options);
		videoFragment.setUrl(path);
		videoFragment.setCallbackContext(callbackContext);

		FrameLayout containerView = (FrameLayout)cordova.getActivity().findViewById(videoViewId);
		if(containerView == null){
			Log.d(TAG, "null video containerView, create new one with ViewId: " + videoViewId);
			final FrameLayout newContainerView = new FrameLayout(cordova.getActivity().getApplicationContext());
			newContainerView.setId(videoViewId);
			FrameLayout.LayoutParams containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
			cordova.getActivity().addContentView(newContainerView, containerLayoutParams);
			containerView = newContainerView;
		}

		videoView = containerView;
		//videoView.setBackgroundColor(Color.BLACK);
		setLayerOfViews();
		//videoView.setBackgroundColor(Color.TRANSPARENT);

		/*
		webView.getView().setBackgroundColor(Color.RED);
		final WebView wv = (WebView)webView.getView();
		wv.setWebViewClient(new WebViewClient(){
			@Override
			public void onPageFinished(WebView view, String url)
			{
				wv.setBackgroundColor(Color.RED);
				//if (Build.VERSION.SDK_INT >= 11) wv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
			}
		});
		*/

		//add the fragment to the container
		FragmentManager fragmentManager = cordova.getActivity().getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		fragmentTransaction.add(containerView.getId(), videoFragment);
		//fragmentTransaction.commit();

		try {
			fragmentTransaction.commit();
			Log.d(TAG, "VideoFragment transaction commit. OK");
		}catch(Exception e){
			Log.e(TAG, "VideoFragment transaction commit failed" + e.getMessage());

			if(videoFragment == null){
				return;
			}
			fragmentTransaction.remove(videoFragment);
			videoFragment = null;

			callbackContext.error("VideoFragment transaction error");
		}
		return;
	}
}


