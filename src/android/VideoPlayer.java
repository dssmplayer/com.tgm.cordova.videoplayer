package com.tgm.cordova.videoplayer;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.Fragment;

import android.util.DisplayMetrics;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.widget.VideoView;
import android.graphics.Color;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaResourceApi;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface; 
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.ComponentName;
import android.content.Context;
import android.util.AttributeSet;

import java.io.FileInputStream;
import java.io.DataInputStream;
import android.media.AudioTrack;
import android.media.AudioFormat;
import java.io.IOException;
import android.view.View;
import java.util.Timer;
import java.util.TimerTask;
import java.util.List;
import java.util.ArrayList;
import java.lang.Thread;

import android.app.Application;
import android.content.res.Resources;
import android.support.v4.view.ViewCompat;
import org.apache.cordova.CordovaActivity;
import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.view.ViewCompat

public class VideoPlayer{
	protected String TAG = "TgmVideoPlayer";

	private String mUrl = null;
	//private boolean					isFinish				= false;
	private Handler	mHandler = new Handler();
	private VideoFragment rtspFragment = null;
	private VideoFragment videoFragment = null;
	private FrameLayout videoView = null;
	private FrameLayout rtspView = null;
	private CallbackContext rtspDisconnectionCallbackContext = null;		// need global callbackContext to check rtsp diconnection event
	private AppCompatActivity cordovaActivity = null;
	private CordovaWebView webView = null;
	private boolean isRtspEstablished = false;

	private int viewId = 8000;

	// constructor
	public VideoPlayer(final int id, AppCompatActivity activity, CordovaWebView wv) {
		Log.d(this.TAG, "VideoPlayer plugin created, id: " + id);
		cordovaActivity = activity;
		webView = wv;
		viewId = id;
	}

	public int getId(){
		return viewId;
	}

	public boolean getIsRtspEstablished(){
		return isRtspEstablished;
	}

	public void close(){
		FragmentManager fragmentManager = cordovaActivity.getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		if(rtspFragment != null){
			fragmentTransaction.remove(rtspFragment);
			try {
				fragmentTransaction.commit();
			}catch(Exception e){
				Log.e(TAG, "RtspFragment transaction commit failed at stop");
			}
			rtspFragment = null;
			rtspView = null;
		}
		isRtspEstablished = false;

		if(videoFragment != null){
			videoFragment.stopPlayers();
			fragmentTransaction.remove(videoFragment);
			try {
				fragmentTransaction.commit();
			}catch(Exception e){
				Log.e(TAG, "VideoFragment transaction commit failed at stop");
			}

			videoFragment = null;
			videoView = null;
		}
	}

	public boolean execute(String action, CordovaArgs args, CallbackContext callbackContext) throws JSONException {
		Log.d(TAG, "action: " + action);

		if (action.startsWith("play")) {
			String target = args.getString(0);
			final JSONObject options = args.getJSONObject(1);
			Log.d(TAG, "play, target: " + target + ", options: " + options.toString());

			CordovaResourceApi resourceApi = webView.getResourceApi();
			
			String fileUriStr;
			try {
				Uri targetUri = resourceApi.remapUri(Uri.parse(target));
				fileUriStr = targetUri.toString();
			} catch (IllegalArgumentException e) {
				fileUriStr = target;
			}

			final String path = stripFileProtocol(fileUriStr);

//			cordovaActivity.runOnUiThread(new Runnable() {
//				public void run() {
					if(action.equals("playRtspPlayer")){
						startPlayWithNewRtspPlayer(path, options, callbackContext);
					}else{
						startPlayWithNewVideoPlayer(path, options, callbackContext);
					}
//				}
//			});
			return true;
		} else if (action.startsWith("close")) {
//			cordovaActivity.runOnUiThread(new Runnable() {
//				public void run() {
					if(action.equals("closeRtspPlayer")){
						stopPlayWithNewRtspPlayer(callbackContext);
					}else{
						stopPlayWithNewVideoPlayer(callbackContext);
					}
//				}
//			});

			return true;
		}else if(action.equals("seekTo")){

			int msec = args.getInt(0);
			long triggerTimeMS = args.getLong(1);

			if(videoFragment != null){
//				cordovaActivity.runOnUiThread(new Runnable() {
//					public void run() {
						try{
							videoFragment.seekTo(msec, triggerTimeMS, callbackContext);
						}catch(Exception e){
							callbackContext.error(e.getMessage());
						}
//					}
//				});
			}else{
				callbackContext.error("videoFragment is null");
			}
			return true;
		}else if(action.equals("isRtspDisconnected")){
			PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
			pluginResult.setKeepCallback(true);
			callbackContext.sendPluginResult(pluginResult);
			rtspDisconnectionCallbackContext = callbackContext;

			Log.d(TAG, "start check rtsp connection.");
			return true;
		}else if(action.equals("changeVideoSource")){
//			cordovaActivity.runOnUiThread(new Runnable() {
//				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.changeVideoSource(args.getString(0), callbackContext);
						}else{
							callbackContext.error("videoFragment is null");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
//				}
//			});
			return true;
		}else if(action.equals("hide")){
//			cordovaActivity.runOnUiThread(new Runnable() {
//				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.hide(callbackContext);
						}else{
							callbackContext.success("videoFragment is null, anyway, hide success");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
//				}
//			});
			return true;
		}else if(action.equals("show")){
//			cordovaActivity.runOnUiThread(new Runnable() {
//				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.show(callbackContext);
						}else{
							callbackContext.success("videoFragment is null, anyway, show success");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
//				}
//			});
			return true;
		}else if(action.equals("gotoStartingPoint")){
//			cordovaActivity.runOnUiThread(new Runnable() {
//				public void run() {
					try{
						if(videoFragment != null){
							videoFragment.gotoStartingPoint(callbackContext);
						}else{
							callbackContext.success("videoFragment is null, anyway, gotoStartingPoint success");
						}
					}catch(Exception e){
						e.printStackTrace();
						callbackContext.error(e.getMessage());
					}
//				}
//			});
			return true;
		}else if(action.equals("isOK")){
			callbackContext.success(isRtspEstablished ? 1 : 0);
			return true;
		}

		return false;
	}

	public static String stripFileProtocol(String uriString) {
		if (uriString.startsWith("file://")) {
			return Uri.parse(uriString).getPath();
		}
		return uriString;
	}

	///////////////////////
	//  new 
	//////////////////////

	synchronized void stopPlayWithNewRtspPlayer(CallbackContext callbackContext){
		if(rtspFragment == null){
			callbackContext.success("not RTSP Player running");
			return;
		}
		FragmentManager fragmentManager = cordovaActivity.getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		fragmentTransaction.remove(rtspFragment);
		fragmentTransaction.commit();
		rtspFragment = null;
		rtspView = null;
		isRtspEstablished = false;

		callbackContext.success("stopRtspPlayer OK");
	}

	synchronized void startPlayWithNewRtspPlayer(String path, JSONObject options, CallbackContext callbackContext){

		if (rtspFragment != null) {
			callbackContext.error("Camera already started");
			return;
		}
		rtspFragment = new VideoFragment();

		rtspFragment.setListener(new VideoFragment.VideoFragmentListener(){
			public void onFailed() {
				Log.d(TAG, "callback VideoFragment rtsp onFailed");
				callbackContext.error("RTSP Action onFailed");
				if(rtspDisconnectionCallbackContext != null){
					rtspDisconnectionCallbackContext.error("RTSP Action onFailed & onClose");
					rtspDisconnectionCallbackContext = null;
					isRtspEstablished = false;
				}
			}
			public void onClose() {
				Log.d(TAG, "callback VideoFragment rtsp onClose");
				//callbackContext.error("RTSP Action onClose");
				if(rtspDisconnectionCallbackContext != null){
					rtspDisconnectionCallbackContext.error("RTSP Action onClose");
					rtspDisconnectionCallbackContext = null;
					isRtspEstablished = false;
				}
			}
			public void onSuccess() {
				Log.d(TAG, "callback VideoFragment rtsp onSuccess");
				callbackContext.success("VideoFragment success");
				isRtspEstablished = true;
			}
		});

		rtspFragment.applyDisplayOptions(options);
		rtspFragment.setUrl(path);

		FrameLayout containerView = (FrameLayout)cordovaActivity.findViewById(viewId);
		if(containerView == null){
			Log.d(TAG, "null rtsp containerView, create new one with ViewId: " + viewId);
			final FrameLayout newContainerView = new FrameLayout(cordovaActivity.getApplicationContext());
			newContainerView.setId(viewId);
			FrameLayout.LayoutParams containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
			cordovaActivity.addContentView(newContainerView, containerLayoutParams);
			containerView = newContainerView;
		}

		rtspView = containerView;

		//add the fragment to the container
		FragmentManager fragmentManager = cordovaActivity.getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		fragmentTransaction.add(containerView.getId(), rtspFragment);
		//fragmentTransaction.commit();

		try {
			fragmentTransaction.commit();
			Log.d(TAG, "startPlayWithNewRtspPlayer complete. OK");
		}catch(Exception e){
			Log.e(TAG, "RtspFragment transaction commit failed");

			if(rtspFragment == null){
				callbackContext.error("RtspFragment transaction creation error");
				return;
			}
			fragmentTransaction.remove(rtspFragment);
			rtspFragment = null;
			callbackContext.error("RtspFragment transaction error");
			isRtspEstablished = false;
		}
		return;
	}

	synchronized void stopPlayWithNewVideoPlayer(CallbackContext callbackContext){
		if(videoFragment == null){
			callbackContext.success("not Video Player running");
			return;
		}
		FragmentManager fragmentManager = cordovaActivity.getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		videoFragment.stopPlayers();
		fragmentTransaction.remove(videoFragment);

		try {
			fragmentTransaction.commit();
		}catch(Exception e){
			Log.e(TAG, "VideoFragment transaction commit failed at stop");
		}

		videoFragment = null;
		videoView = null;

		callbackContext.success("stopVideoPlayer OK");
	}

	synchronized void startPlayWithNewVideoPlayer(String path, JSONObject options, CallbackContext callbackContext){

		if (videoFragment != null) {
			callbackContext.error("Video already started");
			return;
		}
		videoFragment = new VideoFragment();

		videoFragment.setListener(new VideoFragment.VideoFragmentListener(){
			public void onFailed() {
				Log.e(TAG, "VideoFragment onFailed");
				callbackContext.error("VideoFragment failed");
			}
			public void onClose() {
				Log.d(TAG, "VideoFragment onClose");
				callbackContext.error("VideoFragment close");
			}
			public void onSuccess() {
				Log.d(TAG, "VideoFragment onSuccess");
//callbackContext.success("VideoFragment success");
			}
		});

		videoFragment.applyDisplayOptions(options);
		videoFragment.setUrl(path);
		videoFragment.setCallbackContext(callbackContext);

		FrameLayout containerView = (FrameLayout)cordovaActivity.findViewById(viewId);
		if(containerView == null){
			Log.d(TAG, "null video containerView, create new one with ViewId: " + viewId);
			final FrameLayout newContainerView = new FrameLayout(cordovaActivity.getApplicationContext());
			newContainerView.setId(viewId);
			FrameLayout.LayoutParams containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
			cordovaActivity.addContentView(newContainerView, containerLayoutParams);
			containerView = newContainerView;
		}

		videoView = containerView;

		//add the fragment to the container
		FragmentManager fragmentManager = cordovaActivity.getFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

		fragmentTransaction.add(containerView.getId(), videoFragment);
		//fragmentTransaction.commit();

		try {
			fragmentTransaction.commit();
			Log.d(TAG, "VideoFragment transaction commit. OK");
		}catch(Exception e){
			Log.e(TAG, "VideoFragment transaction commit failed" + e.getMessage());

			if(videoFragment == null){
				return;
			}
			fragmentTransaction.remove(videoFragment);
			videoFragment = null;

			callbackContext.error("VideoFragment transaction error");
		}
		return;
	}
}


