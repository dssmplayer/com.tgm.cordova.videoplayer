var exec = require("cordova/exec");

module.exports = {

    DEFAULT_OPTIONS: {
        volume: 1.0,
        scalingMode: 1
    },

    SCALING_MODE: {
        SCALE_TO_FIT: 1,
        SCALE_TO_FIT_WITH_CROPPING: 2
    },

	// for rtsp
    playRtspPlayer: function (path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "playRtspPlayer", [path, options]);
    },

    closeRtspPlayer: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "closeRtspPlayer", []);
    },

	changeRtspSource: function (path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "changeRtspSource", [path]);
	},

	// for plain video
    playVideoPlayer: function (path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "playVideoPlayer", [path, options]);
    },

    closeVideoPlayer: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "closeVideoPlayer", []);
    },

	changeVideoSource: function (path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "changeVideoSource", [path]);
	},

    play: function (path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "play", [path, options]);
    },

    close: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "close", []);
    },

    snapshot: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "snapshot", []);
    },

    isRtspDisconnected: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "isRtspDisconnected", []);
    },

    seekTo: function (msec, triggerTimeMS, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "seekTo", [msec, triggerTimeMS]);
    },

    hide: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "hide", []);
    },

    show: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "show", []);
    },

    gotoStartingPoint: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerSingle", "gotoStartingPoint", []);
    },

    merge: function () {
        var obj = {};
        Array.prototype.slice.call(arguments).forEach(function(source) {
            for (var prop in source) {
                obj[prop] = source[prop];
            }
        });
        return obj;
    }

};
