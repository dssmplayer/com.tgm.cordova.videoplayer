var exec = require("cordova/exec");

module.exports = {

    DEFAULT_OPTIONS: {
        volume: 1.0,
        scalingMode: 1
    },

    SCALING_MODE: {
        SCALE_TO_FIT: 1,
        SCALE_TO_FIT_WITH_CROPPING: 2
    },

	// for rtsp
    playRtspPlayer: function (path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayer", "playRtspPlayer", [path, options]);
    },

    closeRtspPlayer: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "closeRtspPlayer", []);
    },

	changeRtspSource: function (path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "TgmVideoPlayer", "changeRtspSource", [path]);
	},

	// for plain video
    playVideoPlayer: function (path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayer", "playVideoPlayer", [path, options]);
    },

    closeVideoPlayer: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "closeVideoPlayer", []);
    },

	changeVideoSource: function (path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "TgmVideoPlayer", "changeVideoSource", [path]);
	},

    play: function (path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayer", "play", [path, options]);
    },

    close: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "close", []);
    },

    snapshot: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "snapshot", []);
    },

    isRtspDisconnected: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "isRtspDisconnected", []);
    },

    seekTo: function (msec, triggerTimeMS, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "seekTo", [msec, triggerTimeMS]);
    },

    hide: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "hide", []);
    },

    show: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "show", []);
    },

    gotoStartingPoint: function (successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayer", "gotoStartingPoint", []);
    },

    merge: function () {
        var obj = {};
        Array.prototype.slice.call(arguments).forEach(function(source) {
            for (var prop in source) {
                obj[prop] = source[prop];
            }
        });
        return obj;
    }

};
