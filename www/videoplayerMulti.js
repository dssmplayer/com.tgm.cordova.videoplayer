var exec = require("cordova/exec");

module.exports = {

    DEFAULT_OPTIONS: {
        volume: 1.0,
        scalingMode: 1
    },

    SCALING_MODE: {
        SCALE_TO_FIT: 1,
        SCALE_TO_FIT_WITH_CROPPING: 2
    },

    isOpened: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "isOpened", [id]);
    },

    open: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "open", [id]);
    },

    close: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "close", [id]);
    },

	// for rtsp
    playRtspPlayer: function (id, path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "playRtspPlayer", [id, path, options]);
    },

    closeRtspPlayer: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "closeRtspPlayer", [id]);
    },

	changeRtspSource: function (id, path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "changeRtspSource", [id, path]);
	},

	// for plain video
    playVideoPlayer: function (id, path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "playVideoPlayer", [id, path, options]);
    },

    closeVideoPlayer: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "closeVideoPlayer", [id]);
    },

	changeVideoSource: function (id, path, successCallback, errorCallback) {
		exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "changeVideoSource", [id, path]);
	},

    play: function (id, path, options, successCallback, errorCallback) {
        options = this.merge(this.DEFAULT_OPTIONS, options);
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "play", [id, path, options]);
    },

    snapshot: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "snapshot", [id]);
    },

    isOK: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "isOK", [id]);
    },

    isRtspDisconnected: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "isRtspDisconnected", [id]);
    },

    seekTo: function (id, msec, triggerTimeMS, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "seekTo", [id, msec, triggerTimeMS]);
    },

    hide: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "hide", [id]);
    },

    show: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "show", [id]);
    },

    gotoStartingPoint: function (id, successCallback, errorCallback) {
        exec(successCallback, errorCallback, "TgmVideoPlayerMulti", "gotoStartingPoint", [id]);
    },

    merge: function () {
        var obj = {};
        Array.prototype.slice.call(arguments).forEach(function(source) {
            for (var prop in source) {
                obj[prop] = source[prop];
            }
        });
        return obj;
    }

};
